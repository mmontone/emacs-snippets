;;; sldb-buffers.el --- SLDB buffer mode             -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Add context based evaluation to SLIME buffers. The context is the current SLIME condition context.

;;; Code:

(require 'slime)

(defun sldb-buffers-show-source ()
  "Highlight the frame at point's expression in a source code buffer."
  (interactive)
  (sldb-buffers-show-frame-source (sldb-frame-number-at-point)))

(defun sldb-buffers-goto-source ()
  "Highlight the frame at point's expression in a source code buffer."
  (interactive)
  (sldb-buffers-goto-frame-source (sldb-frame-number-at-point)))

(defvar-local sldb-frame-number nil)

(defun sldb-buffers-show-frame-source (frame-number)
  "Show source for FRAME-NUMBER."
  ;; keep slime-current-thread for later assignment
  (let ((current-thread slime-current-thread))
    (slime-eval-async
        `(swank:frame-source-location ,frame-number)
      (lambda (source-location)
        (slime-dcase source-location
          ((:error message)
           (message "%s" message)
           (ding))
          (t
           (slime-show-source-location source-location t nil)
           ;; slime-show-source-location sets the buffer with the source to current-buffer
           ;; enable the sldb-buffers-mode in the source buffer
           (setq-local sldb-frame-number frame-number)
           ;; assign the current-thread to new buffer
           (setq-local slime-current-thread current-thread)
           (sldb-buffers-mode 1)))))))

(defun sldb-buffers-goto-frame-source (frame-number)
  "Show source for FRAME-NUMBER."
  ;; keep slime-current-thread for later assignment
  (let ((current-thread slime-current-thread))
    (slime-eval-async
        `(swank:frame-source-location ,frame-number)
      (lambda (source-location)
        (slime-dcase source-location
          ((:error message)
           (message "%s" message)
           (ding))
          (t
           (slime-show-source-location source-location t nil)
           ;; slime-show-source-location sets the buffer with the source to current-buffer
           ;; enable the sldb-buffers-mode in the source buffer
           (setq-local sldb-frame-number frame-number)
           ;; assign the current-thread to new buffer
           (setq-local slime-current-thread current-thread)
           (sldb-buffers-mode 1)
	   ;; navigate to the buffer
	   (switch-to-buffer-other-window (current-buffer))
	   ))))))

(defun sldb-buffers-eval-last-expression ()
  "Prompt for an expression and evaluate it in the selected frame."
  (interactive)
  (let ((package (slime-current-package))
        (expr (slime-last-expression))
        (frame sldb-frame-number))
    (slime-eval-async `(swank:eval-string-in-frame ,expr ,frame ,(upcase package))
      (if current-prefix-arg
          'slime-write-string
        'slime-display-eval-result))))

(defvar-local sldb-buffers-mode-enabled nil)

(defvar sldb-buffers-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "C-x C-e") 'sldb-buffers-eval-last-expression)
    (define-key map (kbd "C-c I") 'sldb-buffers-inspect)
    (define-key map (kbd "C-c C-r") 'sldb-buffers-eval-region)
    map))

(define-minor-mode sldb-buffers-mode
  "SLDB buffers minor mode."
  :lighter " SLDB-buffers"
  :keymap sldb-buffers-mode-map
  (setq-local sldb-buffers-mode-enabled t)
  ;;(local-set-key (kbd "C-x C-e") 'sldb-buffers-eval-last-expression)
  )

(defun sldb-buffers-quit ()
  "Disable sldb-buffers mode in all buffers."
  (interactive)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      (when sldb-buffers-mode-enabled
	(sldb-buffers-mode -1)
	(setq-local sldb-buffers-mode-enabled nil)
	(setq-local slime-current-thread t)))))

;; When user quits sldb buffer, disable sldb-buffers mode from all buffers.
(advice-add 'sldb-quit :after #'sldb-buffers-quit)
(advice-add 'sldb-invoke-restart :after #'sldb-buffers-quit)
(advice-add 'sldb-abort :after #'sldb-buffers-quit)
(advice-add 'sldb-continue :after #'sldb-buffers-quit)

;; Replace default sldb-mode show-source behaviour.
;; Redirect to a buffer with sldb-buffers mode enabled.
(advice-add 'sldb-show-source :override 'sldb-buffers-show-source)

(provide 'sldb-buffers)
;;; sldb-buffers.el ends here
