# close-all-parentheses

Command for closing all parenthesis at once

- Version: 0.1
- Authors: Sanel Z. <sanelz@gmail.com>
- Maintainer: Sanel Z. <sanelz@gmail.com>


Command for closing all parenthesis at once.
Code extracted from: https://acidwords.com/posts/2017-10-19-closing-all-parentheses-at-once.html

## Functions

- **close-all-parentheses** (arg)

    Command for closing all parentheses\.
    Works in C\-like modes\.
    ARG is prefix arg to control indentation\.

- **close-all-parentheses*** (arg &optional indent-fn)

    Internal function which does most of the job for closing parens\.
    
    INDENT\-FN is an optional indentation function to use\.
    ARG is command prefix arg\.
    
    See \`close\-all\-parentheses'\.

- **close-all-parentheses-simple** ()

    Simple implementation of command for closing all parentheses at once\.
    
    Doesn't work for C like languages\.  Use \`close\-all\-parentheses' for those\.

