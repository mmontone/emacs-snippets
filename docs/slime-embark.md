# slime-embark

Embark keymap for SLIME Lisp mode

- Version: 0.2
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Embark keymap for SLIME Lisp mode.

## Functions

- **slime-embark-target-identifier-at-point** ()

    Target identifier at point\.
    
    In Emacs Lisp and IELM buffers the identifier is promoted to a
    symbol, for which more actions are available\.  Identifiers are
    also promoted to symbols if they are interned Emacs Lisp symbols
    and found in a buffer in a major mode that is not derived from
    \`prog\-mode' \(this is intended for when you might be reading or
    writing about Emacs\)\.
    
    As a convenience, in Org Mode an initial ' or surrounding == or
    ~~ are removed\.

## Variables

- **slime-embark-target-finders** `'(embark-target-top-minibuffer-completion embark-target-active-region embark-target-text-heading-at-point embark-target-collect-candidate embark-target-completion-at-point embark-target-bug-reference-at-point embark-target-package-at-point embark-target-email-at-point embark-target-url-at-point embark-target-file-at-point embark-target-custom-variable-at-point slime-embark-target-identifier-at-point embark-target-library-file-at-point embark-target-expression-at-point embark-target-sentence-at-point embark-target-paragraph-at-point embark-target-defun-at-point embark-target-prog-heading-at-point)`

- **slime-embark-keymap-alist** `'((file embark-file-map) (library embark-library-map) (environment-variables embark-file-map) (url embark-url-map) (email embark-email-map) (buffer embark-buffer-map) (tab embark-tab-map) (expression slime-embark-expression-map) (identifier slime-embark-identifier-map) (defun slime-embark-defun-map) (symbol slime-embark-symbol-map) #'slime-embark-function-map (minor-mode embark-command-map) (unicode-name embark-unicode-name-map) (bookmark embark-bookmark-map) (region embark-region-map) (sentence embark-sentence-map) (paragraph embark-paragraph-map) (kill-ring embark-kill-ring-map) (heading embark-heading-map) (t embark-general-map))`

