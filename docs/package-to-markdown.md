# package-to-markdown

Create a Markdown document from an Emacs simple package file.

- Version: 0.1
- Requirements: emacs v26, s v1.13.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Create a Markdown document from an Emacs simple package file.

## Functions

- **package-to-markdown** (package-file output-file &optional include-code)

    Create a Markdown OUTPUT\-FILE describing PACKAGE\-FILE\.
    
    If INCLUDE\-SOURCE is T, then include the package source code in the document\.

- **markdown-escape-string** (string)

    Escape markdown STRING\.

