# slime-apropos-completions

SLIME completion using apropos

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


SLIME completion using apropos

## Functions

- **slime-apropos-completions-setup** ()

    Setup SLIME apropos completions\.

- **slime-apropos-completion-at-point** ()

    Complete the symbol at point using apropos\.
    Perform completion similar to \`elisp\-completion\-at\-point'\.

- **slime-apropos-completions** (prefix)

    Return apropos completions for PREFIX\.

