# at

Generic accessing library

- Version: 0.3
- Requirements: emacs v26, s v0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Generic accessing library.

## Macros

- **at-with-syntax** (&rest body)

## Functions

- **at** (object key &rest keys)

- **at-1** (object key)

