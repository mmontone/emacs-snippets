# switchable-completions

Poor man's command for switching completion function and style.

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Let the user switch the completion table to use interactively.
Poor man's commands for switching across completion functions and styles.

## Functions

- **switchable-completion-at-point** ()

    Perform completion on the text around point\.
    The completion method is determined by \`completion\-at\-point\-functions'\.

- **switchable-completion-style-next** ()

    Switch to the next completion\-style\.

- **switchable-completion-next** ()

    Switch to the next completion function\.

