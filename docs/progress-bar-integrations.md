# progress-bar-integrations

Integrations of progress-bar into Emacs.

- Version: 0.4
- Requirements: emacs v27.1, progress-bar v0.5
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Integrates progress-bar into Emacs.

As of this moment, this package overwrites some of the package.el operations
in order to display a progress bar for them; and makes Emacs progress-reporter
work with progress-bars too.

## Customisations

- **progress-bar-replace-progress-reporter** `t` (boolean)

    When enabled, use a progress bar instead of default Emacs progress reporter\.

## Functions

- **progress-bar-around-reporter** (orig reporter value &optional suffix)

- **progress-reporter->progress-bar** (reporter value)

    Convert progress REPORTER and current VALUE to a \`progress\-bar'\.
    If a \`progress\-bar' has already been created, then update its \`current\-step' and return it\.

## Variables

- **progress-reporter-progress-bars** `(make-hash-table :weakness 'key)`

    A map of PROGRESS\-REPORTER instances pointing to PROGRESS\-BAR instances\.

