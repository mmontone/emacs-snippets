# publish-archive-contents

Generate a Markdown file with descriptions of the package of an archive-contents

- Version: 0.4
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Generate a Markdown file with the description of the packages of an archive-contents file.

## Functions

- **publish-archive-contents** (archive-contents-file output-file &optional _format)

    Publish a description of the packages of ARCHIVE\-CONTENTS\-FILE to OUTPUT\-FILE\.
    FORMAT can be :markdown for now\.

