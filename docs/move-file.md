# move-file

Command for moving a file to a new location

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Command for moving a file to another location.

## Functions

- **move-file** (new-location)

    Write this file to NEW\-LOCATION, and delete the old one\.

