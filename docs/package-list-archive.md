# package-list-archive

List the packages of an archive

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Use: M-x `package-list-archive'

## Functions

- **package-list-archive** (archive)

    List the packages of ARCHIVE\.

