# package-list-topic

List packages for a given topic

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


List packages for a given topic.

Use: M-x `package-list-topic'.

## Functions

- **package-list-topic** (topic)

    List packages for TOPIC\.

