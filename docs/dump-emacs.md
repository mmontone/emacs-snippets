# dump-emacs

Command for dumping an Emacs core

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Command for dumping an Emacs core.
Makes Emacs start much faster.

Usage:

Load this package from Emacs init file.
Start Emacs and run `dump-emacs' command.
Start Emacs from the dumped core with the following command:
emacs -q --dump-file ~/.emacs.d/emacs.dump -l ~/.emacs.d/load-path.el

## Functions

- **dump-emacs** ()

    Dump current Emacs config\.

- **dump-load-path** ()

    Dump the load path\.
    Dumped Emacs has trouble recovering the load\-path\.
    This function serializes the load\-path to a file that should be loaded
    after a dumped Emacs starts\.

