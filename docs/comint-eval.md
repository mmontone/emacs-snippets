# comint-eval

Minor mode with commands for comint buffer evaluation

- Version: 0.2
- Authors: Marian <marianomontone@gmail.com>
- Maintainer: Marian <marianomontone@gmail.com>


Commands in a minor mode for evaluating pieces of code from a buffer attached to a comint process.

## Functions

- **comint-eval-toplevel** ()

    Evaluate toplevel expression at point\.

- **comint-eval-region** (start end)

    Evaluate region\.

- **comint-eval-last-expr** ()

    Evaluate last expression\.

- **comint-eval-expr** (expr)

    Evaluate EXPRession\.

## Variables

- **comint-eval-toplevel-functions** `'((lisp-mode . comint-eval--list-toplevel-expr))`

    Functions to obtain toplevel expression at point per major\-mode\.

- **comint-eval-default-last-expr-function** `'comint-eval--default-last-expr`

- **comint-eval-last-expr-functions** `'((emacs-lisp-mode . elisp--preceding-sexp) (lisp-mode . elisp--preceding-sexp))`

    Functions to obtain last expression per major\-mode\.

