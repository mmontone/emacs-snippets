# progress-bar

A progress bar in the echo area

- Version: 0.5
- Requirements: emacs v27.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


A progress bar in the echo area.

This package contains the basic implementation.  For integration of progress-bar
into common Emacs commands and behaviors, install progress-bar-integrations package.

Usage:

The preferred method for using a progress-bar is via the utility functions:
`dolist-with-progress-bar', `dotimes-with-progress-bar' and `mapc-with-progress-bar'.

Example:

(dolist-with-progress-bar (x (cl-loop for i from 1 to 10 collect i)
                             :status-message (list "Started ..."
                                                   (lambda (pb)
                                                     (format "Processing %s..." (progress-bar-data pb)))
                                                   "Completed!"))
    (sit-for (seq-random-elt '(0.3 0.4 0.5))))

TODO:
- Consider putting event notification in call-with-progress-bar instead of in the utilities.
- Consider implementing progress-bars with no total-steps specified.
- Consider an option for hiding the progress-bar display after N seconds after completion.

## Customisation groups

- **progress-bar** 

    Progress bar settings\.

## Customisations

- **progress-bar-message-display-layout** `'concatenate` ((choice (const concatenate) (const newline) (const dynamic)))

    How to display messages when in a progress bar scope\.
    If \`concatenate', the message is concatenated to the right of the progress bar\.
    If \`newline', the message is inserted after a new line\.
    If \`dynamic', the message is either concatenated or inserted after a new line
    depending on its length\.

- **progress-bar-min-change** `1` (integer)

    The minimum percentage change required between progress bar displays\.

- **progress-bar-min-time** `0.2` (float)

    The minimum time interval between progress bar displays\.

- **progress-bar-format-string** ` [%d of %d](%d%%%%)` (string)

    String for formatting the progress bar\.
    Arguments passed are current\-step, total\-steps and completed percentage\.
    Consider using field number arguments for more flexibility\.
    See \`format' documentation\.

- **progress-bar-display-after-seconds** `0` (float)

    Display progress bars only after this number of seconds have passed\.

- **progress-bar-min-steps** `0` (integer)

    Minimum number of steps for progress bars to be displayed\.

- **progress-bar-width** `35` (integer)

    Standard width for progress bars\.

- **progress-bar-background-char** `9617` (character)

    Character for drawing progress bars background\.

- **progress-bar-char** `9619` (character)

    Character for drawing progress bars\.

## Macros

- **dotimes-with-progress-bar** (spec &rest body)

    Like \`dotimes' but with a progress bar\.

- **dolist-with-progress-bar** (spec &rest body)

    Like DOLIST but displaying a progress\-bar as items in the list are processed\.
    ARGS are arguments for \`make\-progress\-bar'\.
    
    \(fn \(VAR LIST ARGS\.\.\.\) BODY\.\.\.\)
    
    Example:
    
    \(dolist\-with\-progress\-bar
       \(x \(cl\-loop for i from 1 to 30 collect i\)
          :status\-message "Working \.\.\."\)
       \(sit\-for 0\.3\)\)

- **with-progress-bar** (spec &rest body)

    Create a PROGRESS\-BAR binding SPEC in BODY scope\.
    SPEC has either the form \(VAR PROGRESS\-BAR\-INSTANCE\) or \(VAR &rest INITARGS\), with
    INITARGS used for creating a \`progress\-bar'\.
    This macros sets up special treatment for calls to MESSAGE that may ocurr in BODY,
    so that messages are displayed together with the progress bar\.

## Functions

- **mapc-with-progress-bar** (func sequence &rest args)

    Like \`mapc' but using a progress\-bar\.

- **call-with-progress-bar** (progress-bar func)

    Call FUNC using PROGRESS\-BAR\.
    Sets up special treatment for calls to MESSAGE that may occur when
    evaluating FUNC, so that messages are displayed together with the progress bar\.

- **progress-bar-formatted-status-message** (progress-bar)

    Get formatted status message of PROGRESS\-BAR\.

- **progress-bar-percentage** (progress-bar)

    Current completion percentage of PROGRESS\-BAR\.

- **progress-bar-incf** (progress-bar &optional increment display)

    Increment step in PROGRESS\-BAR\.

- **progress-bar-update** (progress-bar &rest args)

    Update PROGRESS\-BAR and display it\.
    ARGS is a property\-list of slot\-name and value\.
    
    Example:
    \(progress\-bar\-update pg 'current\-step 2 'data 'foo\)

- **progress-bar-notify** (event progress-bar)

    Notify EVENT for PROGRESS\-BAR\.
    See \`progress\-bar\-update\-functions' hook\.

- **progress-bar-completed-p** (progress-bar)

    Return T if PROGRESS\-BAR has completed\.

- **progress-bar-starting-p** (progress-bar)

    Return T if PROGRESS\-BAR is starting and has not yet processed any element\.

## Variables

- **progress-bar-update-functions** `'nil`

    An abnormal hook for getting notified of progress bar updates\.
    Functions get called with a progress bar event, and a progress\-bar instance\.
    Progress bar events can be either \`started', \`updated' or \`completed'

