# progress-buffer

Package for displaying progress of tasks.

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Package for displaying progress of tasks.

## Customisation groups

- **progress-buffer** 

    Progress buffer settings\.

## Customisations

- **progress-buffer-idle-delay** `0.5` (number)

    Number of seconds of idle time to wait before updating progress buffer\.

## Functions

- **progress-buffer-mapc-with-task** (func sequence task-name &rest args)

- **progress-buffer-task-update** (task)

- **progress-buffer-stop** ()

- **progress-buffer-start** ()

- **progress-buffer-open-buffer** ()

- **progress-buffer-make-task** (name &optional status total-steps)

## Variables

- **progress-buffer-progress-bar-size** `50`

- **progress-buffer-progress-bar-background-char** `9617`

- **progress-buffer-progress-bar-char** `9608`

- **progress-buffer-timer** `nil`

- **progress-buffer-tasks** `'nil`

