# sldb-buffers

SLDB buffer mode

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Add context based evaluation to SLIME buffers. The context is the current SLIME condition context.

## Functions

- **sldb-buffers-quit** ()

    Disable sldb\-buffers mode in all buffers\.

- **sldb-buffers-eval-last-expression** ()

    Prompt for an expression and evaluate it in the selected frame\.

- **sldb-buffers-goto-frame-source** (frame-number)

    Show source for FRAME\-NUMBER\.

- **sldb-buffers-show-frame-source** (frame-number)

    Show source for FRAME\-NUMBER\.

- **sldb-buffers-goto-source** ()

    Highlight the frame at point's expression in a source code buffer\.

- **sldb-buffers-show-source** ()

    Highlight the frame at point's expression in a source code buffer\.

## Variables

- **sldb-buffers-mode-map** `(let ((map (make-keymap))) (define-key map (kbd C-x C-e) 'sldb-buffers-eval-last-expression) (define-key map (kbd C-c I) 'sldb-buffers-inspect) (define-key map (kbd C-c C-r) 'sldb-buffers-eval-region) map)`

