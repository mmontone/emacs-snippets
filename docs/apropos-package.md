# apropos-package

Apropos command for searching Emacs packages

- Version: 0.3
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Apropos command for searching Emacs packages.

## Functions

- **apropos-package** (string)

    Search STRING in package archive list\.

