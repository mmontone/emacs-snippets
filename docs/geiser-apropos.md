# geiser-apropos

Apropos for Geiser

- Version: 0.2
- Requirements: emacs v26, geiser v0.29
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Apropos for Geiser.

## Functions

- **geiser-apropos** (what)

    Apropos WHAT\.

- **geiser-eval-expression** (expr)

    Evaluate Scheme EXPR interactively\.

