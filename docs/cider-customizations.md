# cider-customizations

Customizations for CIDER (Clojure mode)

- Version: 0.3
- Requirements: emacs v26, cider v1.5.0, s v1.13.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Customizations for CIDER (Clojure tool).

## Functions

- **cider-eldoc-format-function** (thing pos eldoc-info)

    Return the formatted eldoc string for a function\.
    THING is the function name\.  POS is the argument\-index of the functions
    arglists\.  ELDOC\-INFO is a p\-list containing the eldoc information\.

- **cider-clj-inspector-inspect** (&optional expr inspector-type)

- **cider-clj-inspector-inspect-tree** (&optional expr)

- **cider-clj-inspector-inspect-table** (&optional expr)

- **cider-clj-inspector-inspect-last-result** ()

- **cider-handle-compilation-errors** (message eval-buffer)

    Highlight and jump to compilation error extracted from MESSAGE\.
    EVAL\-BUFFER is the buffer that was current during user's interactive
    evaluation command\.  Honor \`cider\-auto\-jump\-to\-error'\.

- **cider-display-error-message** (message eval-buffer)

- **cider-complete-apropos** (query)

- **cider-docview-render-info** (buffer info)

    Emit into BUFFER formatted INFO for the Clojure or Java symbol\.

