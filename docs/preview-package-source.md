# preview-package-source

Preview package source before installing

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Preview package sources before installing.

When on a package description buffer, run M-x `preview-package-source' command
to visualize the package sources without downloading and installing the package.

## Functions

- **preview-package-source** ()

    Browse to the current package source \(from package description buffers\)\.

