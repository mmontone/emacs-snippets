# log-buffer

A buffer for displaying logs

- Version: 0.4
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


A buffer for displaying logs.

![Log and inspector](./log-buffer.png)

Usage:

Use (log-buffer-log level message [details] options...)

To log to different buffers, bind `log-buffer-name'. For example:

   (let ((log-buffer-name "*my-log*"))
      (log-buffer-log 'info "Hello"))

Create a logger for your application. To do that, pass a list
with application name plus log-level to log-buffer-log:

   (log-buffer-log '(my-app info) "Hello")

Loggers are created disabled by default, so you'll need to enable them
via log-buffer-logger-enable for the log buffer to be created.

To see the log buffer in action, evaluate: M-x log-buffer-test

## Customisations

- **log-buffer-default-options** `'(locate)` ((list symbol))

    Default options passed to \`log\-buffer\-log'\.

- **log-buffer-store-details-in-log-levels** `'(debug)` ((list symbol))

    The log\-levels in which to store details\.

- **log-buffer-entry-item-prefix** `* ` (string)

    Prefix to use for log entries items\.

- **log-buffer-show-details** `nil` (boolean)

    Wether to show details when new entries are added\.

- **log-buffer-details-indent** `4` (integer)

    Indent size for log entry details\.

- **log-buffer-log-level-faces** `'((debug . log-buffer-debug) (info . log-buffer-info) (warning . log-buffer-warning) (error . log-buffer-error))` ((list (cons symbol symbol)))

    Face assignments to log buffer entry level\.

- **log-buffer-log-level-order** `'(debug info warning error)` ((list symbol))

    Order of log levels\.

- **log-buffer-display-when-created** `nil` (boolean)

    Wether to display the log buffer when it is created\.

## Functions

- **log-buffer-logger-test** ()

- **log-buffer-test** (&optional count)

    A test for log\-buffer\.

- **log-buffer-backward-log-entry** (&optional (entry (log-buffer--entry-at-point)))

    Move cursor to next log entry\.

- **log-buffer-forward-log-entry** (&optional (entry (log-buffer--entry-at-point)))

    Move cursor to next log entry\.

- **log-buffer-set-log-level-at-point** ()

    Set buffer log level to the log level of the entry at point\.

- **log-buffer-hide-level-at-point** ()

    Hide entries with log level at point\.

- **log-buffer-inspect-entry** ()

    Inspect log entry at point\.

- **log-buffer-clear** ()

- **log-buffer-hide-others-details** ()

    Hide other entries details

- **log-buffer-hide-details** ()

- **log-buffer-show-details** ()

- **log-buffer-toggle-details** ()

- **log-buffer-toggle-entry-details** ()

- **log-buffer-toggle-show-details** ()

- **log-buffer-disable-logging** ()

- **log-buffer-enable-logging** ()

- **log-buffer-toggle-logging** ()

- **log-buffer-filter-string** (string)

    Filter log entries that contain STRING\.

- **log-buffer-reset-log-level** ()

- **log-buffer-set-log-level** (level)

    Only show entries of a certain level\.

- **log-buffer-log** (level message &optional details &rest options)

    Log MESSAGE in LEVEL including DETAILS to current log buffer\.
    LEVEL can be either a list with logger\-name and log\-level or a
    log\-level, with a log\-level being either symbol or a string, in normal
    practice being one of \`debug', \`info', \`warning' or \`error'\.
    If \`format' is passed in OPTIONS, then final MESSAGE is formated using
    FORMAT with DETAILS as arguments\.  If \`store' is passed in OPTIONS,
    then DETAILS are stored in the log entry and can be later inspected
    using \`inspector' tool from log buffer\.  If \`locate' is part of the
    OPTIONS then the caller function is registered in the log entry and
    can be later navigated to from the log buffer\.

- **log-buffer-log-level** ()

- **log-buffer-logging-enabled** ()

- **log-buffer-create** (&optional logger)

    Create a log buffer\.

- **log-buffer-logger-set-log-level** (logger-name level)

- **log-buffer-logger-disable** (logger-name)

- **log-buffer-logger-enable** (logger-name)

- **log-buffer-find-logger** (name &optional error-p)

- **log-buffer-make-logger** (name &optional enabled level buffer-name)

    Create a logger object\.

## Variables

- **log-buffer-tool-bar-map** `(let ((map (make-sparse-keymap))) (tool-bar-local-item connect 'log-buffer-enable-logging 'log-buffer-enable-logging map :help Enable logging) (tool-bar-local-item disconnect 'log-buffer-disable-logging 'log-buffer-disable-logging map :help Disable logging) (tool-bar-local-item describe 'log-buffer-toggle-details 'log-buffer-toggle-details map :help Toggle details) (tool-bar-local-item down 'log-buffer-forward-log-entry 'log-buffer-forward-log-entry map :help Next log entry) (tool-bar-local-item up 'log-buffer-backward-log-entry 'log-buffer-backward-log-entry map :help Previous log entry) (tool-bar-local-item sort-criteria 'log-buffer-set-log-level 'log-buffer-set-log-level map :help Set log level) (tool-bar-local-item search 'log-buffer-filter-string 'log-buffer-filter-string map :help Filter string) (tool-bar-local-item delete 'log-buffer-clear 'log-buffer-clear map :help Clear log) (tool-bar-local-item preferences 'customize-mode 'customize-mode map :help Customize) (tool-bar-local-item help 'describe-mode 'describe-mode map :help Help) (tool-bar-local-item exit 'kill-buffer 'kill-buffer map :help Quit) map)`

- **log-buffer-mode-map** `(let ((map (make-keymap))) (keymap-set map RET #'log-buffer-toggle-entry-details) (keymap-set map TAB #'log-buffer-toggle-entry-details) (keymap-set map <double-mouse-1> #'log-buffer-toggle-entry-details) (keymap-set map C-<tab> #'log-buffer-toggle-details) (keymap-set map C-n #'log-buffer-forward-log-entry) (keymap-set map C-p #'log-buffer-backward-log-entry) (keymap-set map i #'log-buffer-inspect-entry) map)`

## Faces

- **log-buffer-default** 

    Log buffer face for level entries\.

- **log-buffer-info** 

    Log buffer face for info level entries\.

- **log-buffer-error** 

    Log buffer face for error level entries\.

- **log-buffer-warning** 

    Log buffer face for warning level entries\.

- **log-buffer-debug** 

    Log buffer face for debug level entries\.

