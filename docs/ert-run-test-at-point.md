# ert-run-test-at-point

Run ERT test at point

- Version: 0.3
- Authors: Marian <marianomontone@gmail.com>
- Maintainer: Marian <marianomontone@gmail.com>


Run ERT test at point.

Usage:
Run M-x `ert-run-test-at-point` command when the cursor is over an ERT test.
The individual test at point is run. This is very useful when writing new tests.
Better yet, bind the command to a C-c C-t key, with M-x local-set-key.

## Functions

- **ert-run-test-at-point** ()

    Run the ert test at point\.

