# scmindex

Commands for accessing index.scheme.org

- Version: 0.7
- Requirements: emacs v26, request v0.3.3, at v0.3
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Commands for accessing index.scheme.org

You may want to add key bindings to scheme-mode:

(add-hook 'scheme-mode-hook
         (lambda ()
           (local-set-key (kbd "C-c C-d") 'scmindex-describe-symbol)))


## Customisation groups

- **scmindex** 

    Scheme index settings\.

## Customisations

- **scmindex-url** `https://index.scheme.org` (string)

    Scheme index url\.

- **scmindex-filterset** `r7rs_all` ((choice (const r7rs_all) (const r7rs_small) (const r5rs) (const r6rs) (const r6rs_all)))

    Scheme index filter set\.

## Functions

- **scmindex-apropos** (query)

    Show all meaningful Scheme symbols whose names match QUERY\.

- **scmindex-describe-symbol** (symbol)

    Describe Scheme SYMBOL at point\.

- **scmindex-escape** (x)

