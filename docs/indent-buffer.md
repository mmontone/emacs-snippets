# indent-buffer

Command for indenting the whole buffer

- Version: 0.1
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Command for indenting the whole buffer.

## Functions

- **load-buffer-file** ()

    Load the current buffer elisp file\.

- **indent-buffer** ()

    Indent whole buffer\.

