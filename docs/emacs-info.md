# emacs-info

Display info about current Emacs

- Version: 0.4
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Display info about current Emacs

## Functions

- **emacs-info** ()

    Show info about current Emacs\.

