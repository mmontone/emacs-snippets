# dflet

dynamically-scoped flet

- Version: 0.1
- Authors: Yann Hodique <yann.hodique@gmail.com>
- Maintainer: Yann Hodique <yann.hodique@gmail.com>


This is bringing back the historical definition of `flet', in all its global
and dynamic splendor.

## Macros

- **adflet** (bindings &rest body)

    Anaphoric version of \`dflet'\. Binds \`this\-fn' to the original
    definition of the function\.

- **dflet** (bindings &rest body)

    Make temporary overriding function definitions\.
    This is an analogue of a dynamically scoped \`let' that operates on the function
    cell of FUNCs rather than their value cell\.
    
    \(fn \(\(FUNC ARGLIST BODY\.\.\.\) \.\.\.\) FORM\.\.\.\)

