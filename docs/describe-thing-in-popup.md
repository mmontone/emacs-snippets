# describe-thing-in-popup

Describe thing at point in a popup

- Version: 0.1
- Requirements: popup v0.5.9
- Authors: Mariano Montone <marianomontone@gmail.com>
- Maintainer: Mariano Montone <marianomontone@gmail.com>


Describe thing at point in a popup

## Functions

- **describe-thing-in-popup** ()

    Describe thing at point with a popup\.

