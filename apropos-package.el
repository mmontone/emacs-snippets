;;; apropos-package.el --- Apropos command for searching Emacs packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, package, utilities, snippet
;; Version: 0.3

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Apropos command for searching Emacs packages.

;;; Code:

(require 'package)

(defun apropos-package--filter (string)
  "Filter package-archive-contents by STRING."
  (let (packages)
    (dolist (package-assoc package-archive-contents)
      (let ((package (cadr package-assoc)))
        (when (or (string-match-p (regexp-quote string) (package-desc-summary package))
                  (string-match-p (regexp-quote string) (prin1-to-string (package-desc-name package))))
          (push package packages))))
    packages))

(defun apropos-package (string)
  "Search STRING in package archive list."
  (interactive "sSearch for package: ")
  ;; Initialize the package system if necessary.
  (unless package--initialized
    (package-initialize t))
  (let ((packages (apropos-package--filter string)))
    (if (null packages)
        (message "No packages")
      (package-show-package-list (mapcar 'package-desc-name packages)))))

(provide 'apropos-package)
;;; apropos-package.el ends here
