;;; preview-package-source.el --- Preview package source before installing  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.1
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Preview package sources before installing.
;;
;; When on a package description buffer, run M-x `preview-package-source' command
;; to visualize the package sources without downloading and installing the package.

;;; Code:

(require 'package)

(cl-defun preview-package-source--buffer-package-desc ()
  "Return the current buffer PACKAGE-DESC.
Look for the install button, and fetch the 'package-desc property."
  (goto-char 0)
  (cl-do ((btn (progn (forward-button 1)
		      (button-at (point)))))
      ((button-get btn 'package-desc)
       (button-get btn 'package-desc))))

(defun preview-package-source ()
  "Browse to the current package source (from package description buffers)."
  (interactive)
  (let* ((pkg-desc (preview-package-source--buffer-package-desc))
	 (location (package-archive-base pkg-desc))
         (file (concat (package-desc-full-name pkg-desc)
                       (package-desc-suffix pkg-desc))))
    (browse-url-emacs (concat location file))))

(provide 'preview-package-source)

;;; preview-package-source.el ends here
