;;; describe-thing-in-popup.el --- Describe thing at point in a popup  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: convenience
;; Version: 0.1
;; Package-Requires: ((popup "0.5.9"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Describe thing at point in a popup

;;; Code:

(require 'popup)

(defun describe-thing-in-popup ()
  "Describe thing at point with a popup."
  (interactive)

  (let* ((thing (symbol-at-point))
         (help-xref-following t)
         (description (with-temp-buffer
                        (help-mode)
                        (describe-symbol thing)
                        (buffer-string))))
    (popup-tip description
               :point (point)
               :around t
               :height 30
               :scroll-bar t
               :margin t)))

(provide 'describe-thing-in-popup)

;;; describe-thing-in-popup.el ends here
