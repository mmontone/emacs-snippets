;;; scmindex.el --- Commands for accessing index.scheme.org  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: convenience, tools
;; Version: 0.7
;; Package-Requires: ((emacs "26") (request "0.3.3") (at "0.3"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Commands for accessing index.scheme.org
;;
;; You may want to add key bindings to scheme-mode:
;;
;; (add-hook 'scheme-mode-hook
;;          (lambda ()
;;            (local-set-key (kbd "C-c C-d") 'scmindex-describe-symbol)))
;;
;;; Code:

(require 'request)
(require 'thingatpt)
(require 'json)
(require 'at)
(require 'url-util)

(defgroup scmindex nil
  "Scheme index settings."
  :group 'tools)

(defcustom scmindex-filterset "r7rs_all"
  "Scheme index filter set."
  :type '(choice (const "r7rs_all")
                 (const "r7rs_small")
                 (const "r5rs")
                 (const "r6rs")
                 (const "r6rs_all"))
  :group 'scmindex)

(defcustom scmindex-url "https://index.scheme.org"
  "Scheme index url."
  :type 'string
  :group 'scmindex)

(defmacro scmindex--with-text-properties (properties &rest body)
  `(let ((tp-start (point)))
     ,@body
     (set-text-properties tp-start (point) ,properties)))

(defun scmindex--insert-item (item)
  (when-let ((name  (at item "name")))
    (insert (propertize name 'face 'bold))
    (newline 2)
    (scmindex--insert-signature
     (at item "signature") 'face 'italic)
    (when-let ((desc (at item "description")))
      (newline)
      (insert desc))))

(defun scmindex-escape (x)
  (let ((string (if (symbolp x) (symbol-name x) x)))
    (url-hexify-string string url-path-allowed-chars)))

(defun scmindex-describe-symbol (symbol)
  "Describe Scheme SYMBOL at point."

  (interactive (list
                (if (and (not current-prefix-arg) (symbol-at-point))
                    (symbol-at-point)
                  (intern (read-string "Describe Scheme symbol: ")))))

  (unless symbol
    (user-error "No symbol to describe"))

  (let* ((response (request (concat scmindex-url
                                    "/rest/filterset/"
                                    (scmindex-escape scmindex-filterset)
                                    "/search?query="
                                    (scmindex-escape symbol)
                                    "&facet=false&rows=1")
                     :sync t))
         (data (request-response-data response))
         (json (json-parse-string data))
         (items (at json "items")))
    (if (zerop (length items))
        (message "No description found for: %s" symbol)
      (let ((item (aref items 0)))
        (if (not (string= (symbol-name symbol) (at item "name")))
            (message "No description found for: %s" symbol)
          (with-current-buffer (get-buffer-create (format "*scmindex describe: %s*" (symbol-name symbol)))
            (scmindex--insert-item item)
            (newline 2)
            (goto-char 0)
            (setq buffer-read-only t)
            (local-set-key (kbd "q") 'kill-this-buffer)
            (toggle-word-wrap)
            (pop-to-buffer (current-buffer))))))))

(defun scmindex-apropos (query)
  "Show all meaningful Scheme symbols whose names match QUERY."

  (interactive "sScheme index apropos: ")

  (let* ((response (request (concat scmindex-url
                                    "/rest/filterset/"
                                    (scmindex-escape scmindex-filterset)
                                    "/search?query="
                                    (scmindex-escape query)
                                    "&facet=false")
                     :sync t))
         (data (request-response-data response))
         (json (json-parse-string data))
         (items (at json "items")))
    (with-current-buffer (get-buffer-create (format "*scmindex apropos: %s*" query ))
      (cl-map 'list (lambda (item)
                      (when-let ((name (at item "name")))
                        (insert-button name
                                       'face 'apropos-symbol
                                       'action (lambda (_btn)
                                                 (scmindex-describe-symbol (intern name))))
                        (newline)
                        (insert "  ")
                        (insert (cl-subseq (at item "description")
                                           0
                                           (cl-position ?. (at item "description"))))
                        (newline 2)))
              items)
      (goto-char 0)
      (setq buffer-read-only t)
      (local-set-key (kbd "q") 'kill-this-buffer)
      (pop-to-buffer (current-buffer)))))

;; following: https://github.com/schemeorg-community/index.scheme.org/blob/master/client/cli/src/scmindex.ts

(defun scmindex--param-to-string (param)
  (if (not (zerop (length (at param "types"))))
      (format "[%s %s]"
              (s-join "/" (at param "types"))
              (at param "name"))
    (at param "name")))

(defun scmindex--return-to-string (ret)
  (if (string= (at ret "kind") "return")
      (at ret "type")
    (format "(%s %s)" (at ret "kind")
            (s-join " "
                    (cl-mapcar #'scmindex--return-to-string (at ret "items"))
                    ))))

(defun scmindex--insert-signature (signature &rest properties)
  (cond
   ((string= (at signature "type") "function")
    (apply #'scmindex--insert-function signature properties))
   ((string= (at signature "type") "syntax")
    (apply #'scmindex--insert-syntax signature properties))
   (t (error "TODO"))))

(defun scmindex--insert-function (signature &rest properties)
  (scmindex--with-text-properties
   properties
   (cl-mapc (lambda (variant)
              (let ((params (s-join " " (cl-mapcar #'scmindex--param-to-string (at variant "params"))))
                    (ret (scmindex--return-to-string (at variant "return"))))
                (insert (format "(lambda (%s)) => %s" params ret))
                (newline)))
            (at signature "variants"))))

(defun scmindex--insert-syntax (signature &rest properties)
  (scmindex--with-text-properties
   properties
   (when (not (zerop (length (at signature "literals"))))
     (insert (format "Literals: %s"
                     (s-join ", " (at signature "literals"))))
     (newline))
   (cl-mapc (lambda (pattern)
              (insert (at pattern "pattern"))
              (when-let ((ptype (at pattern "type")))
                (insert (format " => %s" ptype)))
              (newline))
            (at signature "patterns"))))

(provide 'scmindex)

;;; scmindex.el ends here
