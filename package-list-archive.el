;;; package-list-archive.el --- List the packages of an archive  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: convenience
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Use: M-x `package-list-archive'

;;; Code:

(require 'package)

(defun package-list-archive (archive)
  "List the packages of ARCHIVE."
  (interactive (list (completing-read "Package archive: "
                                      (mapcar 'car package-archives))))

  (let ((archive-packages
         (cl-remove-if-not (lambda (pck-desc)
                             (cl-equalp (package-desc-archive pck-desc)
                                        archive))
                           (mapcar #'cadr package-archive-contents))))
    (package-show-package-list (mapcar 'package-desc-name archive-packages))))

(provide 'package-list-archive)
;;; package-list-archive.el ends here
