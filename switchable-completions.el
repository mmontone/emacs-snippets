;;; switchable-completions.el --- Poor man's command for switching completion function and style.  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Let the user switch the completion table to use interactively.
;; Poor man's commands for switching across completion functions and styles.

;;; Code:

(require 'cl-lib)
(require 'minibuffer)

(defvar-local switchable-completion-at-point-functions
  nil
  "The completion functions to switch.
If not set (nil), then `switchable-completion-at-point' switches across `completion-at-point-functions'.")
(defvar-local switchable-completion-current nil
  "The current switching completion function.")
(defvar-local switchable-completion-styles nil
  "The completion styles to switch.
If not set (nil), then `switchable-completion-at-point' switches across `completion-styles'.")
(defvar-local switchable-completion-style nil
  "The current switching completion style.")

(defun switchable-completion-next ()
  "Switch to the next completion function."
  (let ((completion-at-point-list (or switchable-completion-at-point-functions
				      completion-at-point-functions)))
    (setq switchable-completion-current
	  (or
	   (when switchable-completion-current
	     (let ((pos
		    (cl-position switchable-completion-current completion-at-point-list)))
	       (when pos
		 (let ((next-pos (mod (1+ pos) (length completion-at-point-list))))
		   (nth next-pos completion-at-point-list)))))
	   (cl-first completion-at-point-list)))))

(defun switchable-completion-style-next ()
  "Switch to the next completion-style."
  (let ((completion-styles-list (or switchable-completion-styles
				    completion-styles)))
    (when switchable-completion-style
      (let ((pos
             (cl-position switchable-completion-style completion-styles-list)))
	(when pos
          (let ((next-pos (mod (1+ pos) (length completion-styles-list))))
            (nth next-pos completion-styles-list)))))))

(defun switchable-completion-at-point ()
  "Perform completion on the text around point.
The completion method is determined by `completion-at-point-functions'."
  (interactive)
  (if current-prefix-arg
      ;; if command is evaluated using a prefix-arg, switch completion style
      (setq-local switchable-completion-style
                  (if switchable-completion-style
                      (switchable-completion-style-next)
                    (cl-first completion-styles)))
    ;; if command is evaluated without prefix argument, switch completion function
    ;; if there's a completion buffer open, then switch
    (if (get-buffer-window "*Completions*")
	(setq-local switchable-completion-current
                    (if switchable-completion-current
			(switchable-completion-next)
                      (cl-first completion-at-point-functions)))
      ;; else, just use current switchable-completion
      (setq-local switchable-completion-current
		  (or switchable-completion-current
		      (cl-first completion-at-point-functions)))))
  (message "Completing with: %s (%s) ..."
           switchable-completion-current
           switchable-completion-style)
  ;; We make use of Emacs's dynamic extent binding capabilities here ;)
  (let ((completion-at-point-functions (list switchable-completion-current))
	(completion-styles (if switchable-completion-style
			       (list switchable-completion-style)
			     completion-styles)))
    (completion-at-point)))

(provide 'switchable-completions)
;;; switchable-completions.el ends here
