;;; slime-apropos-completions.el --- SLIME completion using apropos  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: completion
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; SLIME completion using apropos

;;; Code:

(require 'cl-lib)
(require 'slime)

(defun slime-apropos-completions (prefix)
  "Return apropos completions for PREFIX."
  (let ((slime-current-thread t))
    (slime-eval
     `(cl:mapcar #'cl:string-downcase
		 (cl:mapcar #'cl:prin1-to-string
			    (cl:apropos-list ,prefix nil :external-only))))))

(defun slime-apropos-completion-at-point ()
  "Complete the symbol at point using apropos.
Perform completion similar to `elisp-completion-at-point'."
  (let* ((end (point))
         (beg (slime-symbol-start-pos)))
    (list beg end (completion-table-dynamic #'slime-apropos-completions)
	  :annotation-function nil)))

(defun slime-apropos-completions-setup ()
  "Setup SLIME apropos completions."
;; slime-complete-symbol-function is deprecated and should be nil for this extension to work
  (setq slime-complete-symbol-function nil)
  
  (add-to-list 'slime-completion-at-point-functions
	       'slime-apropos-completion-at-point))

(provide 'slime-apropos-completions)
;;; slime-apropos-completions.el ends here
