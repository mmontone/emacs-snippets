;;; package-to-markdown.el --- Create a Markdown document from an Emacs simple package file.  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, docs
;; Package-Requires: ((emacs "26") (s "1.13.1"))
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Create a Markdown document from an Emacs simple package file.

;;; Code:

(require 'package)
(require 'at)
(require 's)
(require 'subr-x)

(defun pck2md--insert-version (version)
  "Insert VERSION in buffer."
  (insert (s-join "." (cl-mapcar #'prin1-to-string version))))

(defun package-to-markdown (package-file output-file &optional include-code)
  "Create a Markdown OUTPUT-FILE describing PACKAGE-FILE.

If INCLUDE-SOURCE is T, then include the package source code in the document."
  (let* ((pck-desc
          (with-temp-buffer
            (insert-file-contents package-file)
            (package-buffer-info)))
         (extras (package-desc-extras pck-desc)))

    (with-temp-buffer
      (insert "## ") (insert (prin1-to-string (package-desc-name pck-desc)))
      (newline 2)
      (insert (package-desc-summary pck-desc))
      (newline 2)

      (when-let ((version (package-desc-version pck-desc)))
        (insert "- Version: ")
        (pck2md--insert-version version)
        (newline))

      (when-let ((reqs (package-desc-reqs pck-desc)))
        (insert "- Requirements: ")
        (let ((req (car reqs)))
          (insert (prin1-to-string (car req)))
          (insert " v")
          (pck2md--insert-version (cadr req)))
        (dolist (req (cdr reqs))
          (insert ", ")
          (insert (prin1-to-string (car req)))
          (insert " v")
          (pck2md--insert-version (cadr req)))
        (newline))

      (when-let ((authors (at extras :authors)))
        (insert "- Authors: ")
        (let ((author (car authors)))
          (insert (car author))
          (insert " <")
          (insert (cdr author))
          (insert ">"))
        (dolist (author (cdr authors))
          (insert ", ")
          (insert (car author))
          (insert " <")
          (insert (cdr author))
          (insert ">"))
        (newline))

      (when-let ((maintainer (at extras :maintainer)))
        (insert "- Maintainer: ")
        (insert (car maintainer))
        (insert " <")
        (insert (cdr maintainer))
        (insert ">")
        (newline))

      (newline 2)
      (insert (package--get-description pck-desc))

      (write-file output-file))))

(provide 'package-to-markdown)

;;; package-to-markdown.el ends here
