;;; log-buffer.el --- A buffer for displaying logs             -*- lexical-binding: t; -*-

;; Copyright (C) 2025  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools
;; Version: 0.2

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A buffer for displaying logs.

;; Usage:

;; Use (log-buffer-log level message [details] options...)

;; To log to different buffers, bind `log-buffer-name'. For example:

;; (let ((log-buffer-name "*my-log*"))
;;   (log-buffer-log 'info "Hello"))

;; To see the log buffer in action, evaluate: M-x log-buffer-test

;;; Code:

;; TODO:
;; - Add possibility of adding provenance information to log entries and
;;   provide navigation to locations where the log happened.
;; - context menu
;; - optional buffer heading with info like current log-level, search string, log entries cound, etc
;; - show number of log-entries, perhaps number of entries by log-level, in modeline?
;; - completer of log-levels for commands?
;; - set a max log entries number (optional) for a log-buffer, and strip the buffer to that number. possible?
;; - help buffer

(require 'cl-lib)

(defvar-local log-buffer-name "*log*")
(defvar-local log-buffer--entries (list))
(defvar-local log-buffer-action 'log-buffer-inspect)
(defvar-local log-buffer-log-level t
  "The buffer log level.")
(defvar-local log-buffer-logging-enabled t
  "Wether the log buffer accepts new entries.")
(defvar-local log-buffer--filter-string nil
  "The string being used for filtering logs.")

(defcustom log-buffer-display-when-created nil
  "Wether to display the log buffer when it is created."
  :type 'boolean
  :group 'log-buffer)

(defcustom log-buffer-log-level-order
  '(debug info warning error)
  "Order of log levels."
  :type '(list symbol)
  :group 'log-buffer)

(defcustom log-buffer-log-level-faces
  '((debug . log-buffer-debug)
    (info . log-buffer-info)
    (warning . log-buffer-warning)
    (error . log-buffer-error))
  "Face assignments to log buffer entry level."
  :type '(list (cons symbol symbol))
  :group 'log-buffer)

(defcustom log-buffer-details-indent
  4
  "Indent size for log entry details."
  :type 'integer
  :group 'log-buffer)

(defcustom log-buffer-show-details
  nil
  "Wether to show details when new entries are added."
  :type 'boolean
  :group 'log-buffer)

(defcustom log-buffer-entry-item-prefix
  "* "
  "Prefix to use for log entries items."
  :type 'string
  :group 'log-buffer)

(defcustom log-buffer-store-details-in-log-levels
  '(debug)
  "The log-levels in which to store details."
  :type '(list symbol)
  :group 'log-buffer)

(defface log-buffer-debug
  '((t :inherit warning))
  "Log buffer face for debug level entries."
  :group 'log-buffer-faces)

(defface log-buffer-warning
  '((t :inherit warning))
  "Log buffer face for warning level entries."
  :group 'log-buffer-faces)

(defface log-buffer-error
  '((t :inherit error))
  "Log buffer face for error level entries."
  :group 'log-buffer-faces)

(defface log-buffer-info
  ;; Semantically `success' is probably not the right face, but it looks nice as
  ;; a base face
  '((t :inherit success))
  "Log buffer face for info level entries."
  :group 'log-buffer-faces)

(defface log-buffer-default
  '((t :inherit default))
  "Log buffer face for level entries."
  :group 'log-buffer-faces)

(defun log-buffer--face-for-level (level)
  (or (cdr (assoc level log-buffer-log-level-faces))
      'log-buffer-default))

(defun log-buffer-create ()
  "Create a debug log buffer."
  (if (get-buffer log-buffer-name)
      (get-buffer log-buffer-name)
    (let ((buffer (get-buffer-create log-buffer-name)))
      (with-current-buffer buffer
        (log-buffer-mode)
        (setq buffer-read-only t))
      (when log-buffer-display-when-created
        (display-buffer buffer))
      buffer)))

(cl-defun log-buffer-log (level message &optional details &rest options)
  "Log MESSAGE in LEVEL including DETAILS to current log buffer.
LEVEL can be either a symbol or a string, with normal practice
being one of `debug', `info', `warning' or `error'.
If `format' is passed in OPTIONS, then final MESSAGE is formated
using FORMAT with DETAILS as arguments.  if `store' is passed in
OPTIONS, then DETAILS are stored in the log entry and can be later
inspected using `inspector' tool from log buffer."
  (log-buffer-create)
  (with-current-buffer log-buffer-name
    (when (or (not log-buffer-logging-enabled)
              (not (log-buffer--level-satisfies-p level log-buffer-log-level)))
      (cl-return-from log-buffer-log))
    (goto-char (point-max))
    (let ((buffer-read-only nil)
          (entry-beg (point))
          (msg (if (member 'format options)
                   (apply #'format message details)
                 message)))
      (insert log-buffer-entry-item-prefix)
      (let ((point (point)))
        (insert "[")
        (princ level (current-buffer))
        (insert "]")
        (add-face-text-property point (point) (log-buffer--face-for-level level)))
      (insert " ")
      (insert msg)
      (let ((details-beg (point)))
        (when details
          (newline)
          (insert (make-string log-buffer-details-indent ?\s))
          (cond
           ((stringp details)
            (insert-rectangle (split-string details "\n")))
           (t (pp details (current-buffer)))))
        ;; Create an overlay to hide the entry details
        (let ((entry-details (make-overlay details-beg (point))))
          (overlay-put entry-details 'invisible (not log-buffer-show-details))
          ;; Add a log entry to log-buffer--entries
          (push (list :beg entry-beg :end (1+ (point)) :level level
                      :details (when (or (member 'store options)
                                         (member level log-buffer-store-details-in-log-levels))
                                 details)
                      :details-overlay entry-details)
                log-buffer--entries))
        (newline)))))

(cl-defun log-buffer--entry-at-point (&optional (point (point)))
  (dolist (entry log-buffer--entries)
    (when (<= (cl-getf entry :beg) point (cl-getf entry :end))
      (cl-return-from log-buffer--entry-at-point entry)))
  nil)

(defun log-buffer--entry-contents (entry)
  (buffer-substring (plist-get entry :beg)
                    (plist-get entry :end)))

(defun log-buffer--hide-entry (entry)
  ;; Create an overlay to hide the entry
  (let ((hide-overlay (or (plist-get entry :hide-overlay)
                          (let ((ov
                                 (make-overlay (plist-get entry :beg)
                                               (plist-get entry :end))))
                            (plist-put entry :hide-overlay ov)
                            ov))))
    (overlay-put hide-overlay 'invisible t)
    (overlay-put hide-overlay 'intangible t)))

(defun log-buffer--entry-visible-p (entry)
  (let ((hide-overlay (plist-get entry :hide-overlay)))
    (or (not hide-overlay)
        (not (overlay-get hide-overlay 'invisible)))))

(defun log-buffer--show-entry (entry)
  (let ((hide-overlay (plist-get entry :hide-overlay)))
    (when hide-overlay
      (overlay-put hide-overlay 'invisible nil))))

(defun log-buffer--level< (l1 l2)
  (let ((p1 (cl-position l1 log-buffer-log-level-order))
        (p2 (cl-position l2 log-buffer-log-level-order)))
    (and p1 p2 (< p1 p2))))

;; (log-buffer--level< 'debug 'error)

(defun log-buffer--level-satisfies-p (level level-exp)
  "Return T if LEVEL satisfies LEVEL-EXPression."
  (cond
   ((eq level-exp 't) t)
   ((listp level-exp)
    (cl-case (car level-exp)
      (and (cl-every (lambda (exp)
                       (log-buffer--level-satisfies-p level exp))
                     (cdr level-exp)))
      (or (cl-some (lambda (exp)
                     (log-buffer--level-satisfies-p level exp))
                   (cdr level-exp)))
      (not (not (log-buffer--level-satisfies-p level (cadr level-exp))))
      (< (log-buffer--level< level (cadr level-exp)))
      (> (not (log-buffer--level< level (cadr level-exp))))
      (= (cl-equalp level (cadr level-exp)))
      (t (error "Invalid buffer log level: %s" level-exp))))
   (t (cl-equalp level level-exp))))

;; (log-buffer--level-satisfies-p 'debug 'debug)
;; (log-buffer--level-satisfies-p 'debug 't)
;; (log-buffer--level-satisfies-p 'debug '(or debug info))
;; (log-buffer--level-satisfies-p 'info '(or debug info))
;; (log-buffer--level-satisfies-p 'error '(or debug info))

(defun log-buffer--filter ()
  "Filter log buffer entries using `log-buffer-log-level'"
  (dolist (entry log-buffer--entries)
    (if (and (log-buffer--level-satisfies-p (plist-get entry :level) log-buffer-log-level)
             (or (null log-buffer--filter-string)
                 (cl-search log-buffer--filter-string (log-buffer--entry-contents entry))))
        (log-buffer--show-entry entry)
      (log-buffer--hide-entry entry))))

(defun log-buffer-set-log-level (level)
  "Only show entries of a certain level."
  (interactive "sBuffer log level: ")
  (setq-local log-buffer-log-level (car (read-from-string level)))
  (log-buffer--filter))

(defun log-buffer-reset-log-level ()
  (interactive)
  (setq-local log-buffer-log-level t)
  (log-buffer--filter))

(defun log-buffer-filter-string (string)
  "Filter log entries that contain STRING."
  (interactive "sFilter log entries: ")
  (setq-local log-buffer--filter-string string)
  (when (zerop (length string))
    (setq-local log-buffer--filter-string nil))
  (log-buffer--filter))

(defun log-buffer-toggle-logging ()
  (interactive)
  (setq-local log-buffer-logging-enabled (not log-buffer-logging-enabled)))

(defun log-buffer-enable-logging ()
  (interactive)
  (setq-local log-buffer-logging-enabled t))

(defun log-buffer-disable-logging ()
  (interactive)
  (setq-local log-buffer-logging-enabled nil))

(defun log-buffer-toggle-show-details ()
  (interactive)
  (setq-local log-buffer-show-details (not log-buffer-show-details)))

(defun log-buffer--toggle-entry-details (entry)
  (let ((details-overlay (plist-get entry :details-overlay)))
    (overlay-put details-overlay 'invisible (not (overlay-get details-overlay 'invisible)))))

(defun log-buffer--show-entry-details (entry)
  (let ((details-overlay (plist-get entry :details-overlay)))
    (overlay-put details-overlay 'invisible nil)))

(defun log-buffer--hide-entry-details (entry)
  (let ((details-overlay (plist-get entry :details-overlay)))
    (overlay-put details-overlay 'invisible t)))

(defun log-buffer-toggle-entry-details ()
  (interactive)
  (when-let* ((entry (log-buffer--entry-at-point)))
    (log-buffer--toggle-entry-details entry)))

(defun log-buffer-toggle-details ()
  (interactive)
  (dolist (entry log-buffer--entries)
    (log-buffer--toggle-entry-details entry)))

(defun log-buffer-show-details ()
  (interactive)
  (dolist (entry log-buffer--entries)
    (log-buffer--show-entry-details entry)))

(defun log-buffer-hide-details ()
  (interactive)
  (dolist (entry log-buffer--entries)
    (log-buffer--hide-entry-details entry)))

(defun log-buffer-hide-others-details ()
  "Hide other entries details"
  (interactive)
  (when-let* ((entry-at-point (log-buffer--entry-at-point)))
    (dolist (entry log-buffer--entries)
      (when (not (eq entry entry-at-point))
        (log-buffer--hide-entry-details entry)))))

(defun log-buffer-clear ()
  (interactive)
  ;; delete overlays
  (dolist (entry log-buffer--entries)
    (when-let* ((details-overlay (plist-get entry :details-overlay)))
      (delete-overlay details-overlay))
    (when-let* ((hide-overlay (plist-get entry :hide-overlay)))
      (delete-overlay hide-overlay)))
  (setq-local log-buffer--entries nil)
  (let ((buffer-read-only nil))
    (erase-buffer)))

(defun log-buffer-inspect-entry ()
  "Inspect log entry at point."
  (interactive)
  (when-let* ((entry (log-buffer--entry-at-point)))
    (when (functionp 'inspector-inspect)
      (funcall 'inspector-inspect
               (if (plist-get entry :details)
                   (list (log-buffer--entry-contents entry)
                         (plist-get entry :details))
                 (log-buffer--entry-contents entry))))))

(defun log-buffer-hide-level-at-point ()
  "Hide entries with log level at point."
  (interactive)
  (when-let* ((entry (log-buffer--entry-at-point)))
    (let ((level (plist-get entry :level)))
      (setq log-buffer-log-level `(and ,log-buffer-log-level (not ,level))))
    (log-buffer--filter)))

(defun log-buffer-set-log-level-at-point ()
  "Set buffer log level to the log level of the entry at point."
  (interactive)
  (when-let* ((entry (log-buffer--entry-at-point)))
    (let ((level (plist-get entry :level)))
      (setq log-buffer-log-level level))
    (log-buffer--filter)))

(defun log-buffer-log-elisp (level message &optional details)
  "Log with provenance information (current function)."
  (let ((fname (nth 1 (backtrace-frame 2))))
    (log-buffer-log level message (cons (cons :elisp-fname fname)
                                        details))))

(cl-defun log-buffer-forward-log-entry (&optional (entry (log-buffer--entry-at-point)))
  "Move cursor to next log entry."
  (interactive)
  (when-let* ((next-entry (log-buffer--entry-at-point (1+ (plist-get entry :end)))))
    (if (log-buffer--entry-visible-p next-entry)
        (goto-char (plist-get next-entry :beg))
      (log-buffer-forward-log-entry next-entry))))

(cl-defun log-buffer-backward-log-entry (&optional (entry (log-buffer--entry-at-point)))
  "Move cursor to next log entry."
  (interactive)
  (when-let* ((prev-entry (log-buffer--entry-at-point (1- (plist-get entry :beg)))))
    (if (log-buffer--entry-visible-p prev-entry)
        (goto-char (plist-get prev-entry :beg))
      (log-buffer-backward-log-entry prev-entry))))

(defvar log-buffer-mode-map
  (let ((map (make-keymap)))
    (keymap-set map "RET" #'log-buffer-toggle-entry-details)
    (keymap-set map "TAB" #'log-buffer-toggle-entry-details)
    (keymap-set map "<double-mouse-1>" #'log-buffer-toggle-entry-details)
    (keymap-set map "C-<tab>" #'log-buffer-toggle-details)
    (keymap-set map "C-n" #'log-buffer-forward-log-entry)
    (keymap-set map "C-p" #'log-buffer-backward-log-entry)
    (keymap-set map "i" #'log-buffer-inspect-entry)
    map))

(easy-menu-define log-buffer-menu log-buffer-mode-map
  "Menu for log buffer."
  '("Log"
    ["Toggle logging" log-buffer-toggle-logging]
    ["Toggle show details" log-buffer-toggle-show-details]
    ["Set log level" log-buffer-set-log-level]
    ["Set log level at point" log-buffer-set-log-level-at-point :help "Set log level to the level of the entry at point"]
    ["Reset log level" log-buffer-reset-log-level]
    ["Hide details" log-buffer-hide-details]
    ["Show details" log-buffer-show-details]
    ["Hide others details" log-buffer-hide-others-details]
    ["Toggle details" log-buffer-toggle-details]
    ["Filter" log-buffer-filter-string :help "Filter log buffer by a string"]
    ["Clear" log-buffer-clear]))

(defvar log-buffer-tool-bar-map
  (let ((map (make-sparse-keymap)))
    (tool-bar-local-item "connect" 'log-buffer-enable-logging 'log-buffer-enable-logging map :help "Enable logging")
    (tool-bar-local-item "disconnect" 'log-buffer-disable-logging 'log-buffer-disable-logging map :help "Disable logging")
    (tool-bar-local-item "describe" 'log-buffer-toggle-details 'log-buffer-toggle-details map :help "Toggle details")
    (tool-bar-local-item "down" 'log-buffer-forward-log-entry 'log-buffer-forward-log-entry map :help "Next log entry")
    (tool-bar-local-item "up" 'log-buffer-backward-log-entry 'log-buffer-backward-log-entry map :help "Previous log entry")
    (tool-bar-local-item "sort-criteria" 'log-buffer-set-log-level 'log-buffer-set-log-level map :help "Set log level")
    (tool-bar-local-item "search" 'log-buffer-filter-string 'log-buffer-filter-string map :help "Filter string")
    (tool-bar-local-item "delete" 'log-buffer-clear 'log-buffer-clear map :help "Clear log")
    (tool-bar-local-item "preferences" 'customize-mode 'customize-mode map :help "Customize")
    (tool-bar-local-item "exit" 'kill-buffer 'kill-buffer map :help "Quit")
    map))

(define-derived-mode log-buffer-mode special-mode "Log-Buffer-Mode"
  "Major mode for log buffers."
  :interactive nil
  :group 'log-buffer
  (setq-local tool-bar-map log-buffer-tool-bar-map))

(defun log-buffer-test (&optional count)
  "A test for log-buffer."
  (interactive)
  (let ((loggers (list (lambda ()
                         (log-buffer-log 'info "This is information" '((info . "Lalala"))))
                       (lambda ()
                         (log-buffer-log 'debug "This is for debugging" (list 'foo 'bar 'baz)))
                       (lambda ()
                         (log-buffer-log 'error "This is error" '(("error message" . "A horrible error"))))
                       (lambda ()
                         (log-buffer-log 'info "A cool thing" "Lalalal\nBasdfasdf\nCddfdfdf")))))
    (dotimes (_ (or count 1000))
      (funcall (seq-random-elt loggers)))
    (switch-to-buffer "*log*")))

;; (log-buffer-test)

(provide 'log-buffer)

;;; log-buffer.el ends here
