;;; package-docgen.el --- Api documentation generator for single file packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, docs
;; Version: 0.2

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Api documentation generator for single file packages

;;; Code:

(require 'cl-lib)
(require 'find-func)

(defvar markdown--escape-characters (list ?\\ ?` ?* ?_ ?{ ?} ?\( ?\) ?# ?+ ?- ?. ?!))

(defun markdown-escape-string (string)
  "Escape markdown STRING."
  (let ((chars '()))
    (seq-doseq (char string)
      (when (member char markdown--escape-characters)
        (push ?\\ chars))
      (push char chars))
    (apply #'string (nreverse chars))))

;; (markdown-escape-string "**fooo**")

(defun package-docgen--external-symbol-p (symbol)
  "Determines if SYMBOL to be considered \"external\"."
  (not (cl-search "--" (symbol-name symbol))))

;; (describe-symbol 'inspector-action-face)
;; (documentation-property 'inspector-action-face 'face-documentation)
;; (documentation 'inspector-inspect)
;; (documentation 'inspector-inspect-last-sexp)
;; (documentation-property 'inspector-truncation-limit 'variable-documentation)
;; (symbol-plist 'inspector-truncation-limit)
;; (symbol-plist 'inspector-action-face)

(defun package-docgen--symbol-package (symbol)
  "Ad-hoc function for determining the package of SYMBOL."

  )

;; (find-library-name "inspector")
;; (find-function-library 'inspector-inspect)
;; (symbol-file 'inspector-truncation-limit 'defvar)
;; (symbol-file 'inspector-inspect 'defun)
;; (symbol-file 'inspector-inspect-last-sexp 'defun)
;; (symbol-file 'inspector-action-face 'defface)
;; (commandp 'inspector-inspect-last-sexp)

(defun read-from-file (filename callback)
  "Read sexps in FILENAME and calling CALLBACK."
  (condition-case nil
      (with-temp-buffer
        (insert-file-contents filename)
        (goto-char 0)
        (while t
          (let ((sexp (read (current-buffer))))
            (funcall callback sexp))))
    (end-of-file)))

;; (read-from-file (find-library-name "inspector") #'debug)
;; (read-from-file (find-library-name "inspector") #'print)

(defun package-docgen--package-definitions (package-name)
  "List definitions for PACKAGE-NAME."
  (let ((package-file (find-library-name package-name)))
    (let ((defs (list (cons 'defun ())
                      (cons 'defvar ())
                      (cons 'defcustom ())
                      (cons 'defface ())
                      (cons 'defgroup ()))))
      (read-from-file package-file
                      (lambda (sexp)
                        (cl-case (car sexp)
                          (defun (push (cons (cadr sexp) sexp)
                                       (alist-get 'defun defs)))
                          (defvar (push (cons (cadr sexp) sexp)
                                        (alist-get 'defvar defs)))
                          (defcustom (push (cons (cadr sexp) sexp)
                                           (alist-get 'defcustom defs)))
                          (defgroup (push (cons (cadr sexp) sexp)
                                          (alist-get 'defgroup defs)))
                          (defface (push (cons (cadr sexp) sexp)
                                         (alist-get 'defface defs))))))
      defs)))

(defun package-docgen-markdown (package-name filename)
  "Write documentation for PACKAGE-NAME in FILENAME."
  (cl-labels ((insert-doc (doc)
                ;;(insert "```") (newline)
                (insert "    ")
                (insert-rectangle (split-string (markdown-escape-string doc) "\n"))
                ;; (newline) (insert "```")
                )
              (insert-definition-section (section defs)
                (insert "## " (cdr section))
                (newline 2)
                (dolist (def defs)
                  (cl-case (car section)
                    (defun
                        (insert "- **" (prin1-to-string (car def)) "** ")
                        (cl-destructuring-bind (_ _fname fargs &rest fbody) (cdr def)
                          (insert (if (null fargs) "()" (prin1-to-string fargs)))
                          (when (stringp (car fbody))
                            (newline 2)
                            (insert-doc (car fbody)))))
                    (defvar
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _varname &optional initvalue docstring) (cdr def)
                        (insert (format "`%s`" initvalue))
                        (when docstring
                          (newline 2)
                          (insert-doc docstring))))
                    (defgroup
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _groupname _members doc &rest args) (cdr def)
                        (newline 2)
                        (insert-doc doc)))
                    (defcustom
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _varname standard doc &rest args) (cdr def)
                        (insert (format "`%s`" standard))
                        (insert (format " (%s)" (cadr (cl-getf args :type))))
                        (newline 2)
                        (insert-doc doc)))
                    (defface
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _name _spec doc &rest args) (cdr def)
                        (newline 2)
                        (insert-doc doc)))
                    )
                  (newline 2))))
    (let ((defs (package-docgen--package-definitions package-name))
          (sections '((defgroup . "Customisation groups")
                      (defcustom . "Customisations")
                      (defun . "Functions")
                      (defvar . "Variables")
                      (defface . "Faces"))))
      (with-temp-buffer
        (insert "# " package-name)
        (newline 2)
        (dolist (section sections)
          (let ((section-defs (alist-get (car section) defs)))
            (setq section-defs (cl-remove-if-not (lambda (def)
                                                   (package-docgen--external-symbol-p (car def)))
                                                 section-defs))
            (when section-defs
              (insert-definition-section section section-defs))))
        (write-file filename)))))

;; (package-docgen--package-definitions "inspector")
;; (package-docgen-markdown "inspector" "/home/marian/src/inspector.md")
;; (package-docgen--package-definitions "package-docgen")
;; (package-docgen-markdown "package-docgen" "/home/marian/src/package-docgen.md")

(provide 'package-docgen)

;;; package-docgen.el ends here
