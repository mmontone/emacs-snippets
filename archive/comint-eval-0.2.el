;;; comint-eval.el --- Minor mode with commands for comint buffer evaluation  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Marian <marianomontone@gmail.com>
;; Keywords: processes, tools
;; Version: 0.2

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Commands in a minor mode for evaluating pieces of code from a buffer attached to a comint process.

;;; Code:

(require 'comint)
(require 'cl-macs)
(require 'cl-seq)

(defvar-local comint-eval--buffer-process nil
  "The comint process attached to the buffer.")

(defvar comint-eval-last-expr-functions
  '((emacs-lisp-mode . elisp--preceding-sexp)
    (lisp-mode . elisp--preceding-sexp))
  "Functions to obtain last expression per major-mode.")

(defvar comint-eval-default-last-expr-function
  'comint-eval--default-last-expr)

(defvar comint-eval-toplevel-functions
  '((lisp-mode . comint-eval--list-toplevel-expr))
  "Functions to obtain toplevel expression at point per major-mode.")

(defun comint-eval--default-last-expr ()
  "Default implementation of last-expr."
  (let ((start (save-excursion (backward-sexp) (point)))
        (end (point)))
    (buffer-substring-no-properties start end)))

(defun comint-eval--list-comint-buffers ()
  "Return a list with comint buffers."
  (cl-remove-if-not
   (lambda (buf)
     (with-current-buffer buf
       (eq major-mode 'comint-mode)))
   (buffer-list)))

(defun comint-eval--last-expr ()
  "Get string of last expression, depending on the major-mode."
  (if-let (entry (assoc major-mode comint-eval-last-expr-functions))
      (funcall (cdr entry))
    (funcall comint-eval-default-last-expr-function)))

(defun comint-eval-expr (expr)
  "Evaluate EXPRession."
  (interactive "sEval expression: ")
  (comint-send-string comint-eval--buffer-process
                      (concat expr "\n")))

(defun comint-eval-last-expr ()
  "Evaluate last expression."
  (interactive)
  (comint-eval-expr (comint-eval--last-expr)))

(defun comint-eval--region-to-string (begin &optional end)
  (let ((end (or end (point))))
    (when (< begin end)
      (let* ((str (buffer-substring-no-properties begin end))
             (pieces (split-string str nil t)))
        (mapconcat 'identity pieces " ")))))

(defun comint-eval-region (start end)
  "Evaluate region."
  (interactive "r")
  (comint-eval-expr (comint-eval--region-to-string start end)))

(defun comint-eval--lisp-toplevel-expr ()
  "Return Lisp toplevel expression when in Lisp mode."
  (let (beg end form)
    ;; Read the form from the buffer, and record where it ends.
    (save-excursion
      (end-of-defun)
      (beginning-of-defun)
      (setq beg (point))
      (setq form (funcall load-read-function (current-buffer)))
      (setq end (point)))
    (cons beg end)))

(defun comint-eval-toplevel ()
  "Evaluate toplevel expression at point."
  (interactive)

  (let ((region (if-let (entry (assoc major-mode comint-eval-toplevel-functions))
                    (funcall (cdr entry))
                  (comint-eval--lisp-toplevel-expr))))
    (comint-eval-region (car region) (cdr region))))

(defun comint-eval--attach-process ()
  "Attach the comint process to the current buffer
if it is not attached already."
  (let* ((comint-buffers (comint-eval--list-comint-buffers))
         (comint-buffer
          (cl-case (length comint-buffers)
            (0 (user-error "No comint buffer available when trying to start comint-eval-mode"))
            (1 (car comint-buffers))
            (t (let ((buffer-name (completing-read "Select comint buffer: "
                                                   (mapcar #'buffer-name comint-buffers))))
                 (get-buffer buffer-name))))))
    (setq-local comint-eval--buffer-process (get-buffer-process comint-buffer))))

(define-minor-mode comint-eval-mode
  "Toggle comint-eval minor mode."
  ;; Indicator for mode line
  :lighter " comint-eval"
  ;; The minor mode bindings
  :keymap
  `((,(kbd "C-x C-e") . comint-eval-last-expr)
    (,(kbd "C-M-x") . comint-eval-toplevel))
  ;; Start up
  (comint-eval--attach-process))

(provide 'comint-eval)

;;; comint-eval.el ends here
