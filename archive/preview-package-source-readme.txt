Preview package sources before installing.

When on a package description buffer, run M-x `preview-package-source' command
to visualize the package sources without downloading and installing the package.