;;; progress-bar.el --- A progress bar in the echo area              -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, convenience
;; Version: 0.1
;; Package-Requires: ((emacs "27"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A progress bar in the echo area.

;;; Code:

(require 'eieio)

(defgroup progress-bar nil
  "Progress bar settings."
  :group 'convenience)

;; Chosen from https://en.wikipedia.org/wiki/Block_Elements and inserted using `insert-char' command:

(defcustom progress-bar-char ?▓
  "Character for drawing progress bars."
  :type 'character
  :group 'progress-bar)

(defcustom progress-bar-background-char ?░
  "Character for drawing progress bars background."
  :type 'character
  :group 'progress-bar)

(defcustom progress-bar-width 35
  "Standard width for progress bars."
  :type 'integer
  :group 'progress-bar)

(defcustom progress-bar-min-steps 0
  "Minimum number of steps for progress bars to be displayed."
  :type 'integer
  :group 'progress-bar)

(defcustom progress-bar-format-string " [%d of %d](%d%%%%)"
  "String for formatting the progress bar.
Arguments passed are current-step, total-steps and completed percentage.
Consider using field number arguments for more flexibility.
See `format' documentation."
  :type 'string
  :group 'progress-bar)

(defcustom progress-bar-min-time 0.2
  "The minimum time interval between progress bar displays."
  :type 'float
  :group 'progress-bar)

(defcustom progress-bar-min-change 1
  "The minimum percentage change required between progress bar displays."
  :type 'integer
  :group 'progress-bar)

(cl-defstruct progress-bar
  (status-message nil
                  :documentation "The status-message should be either a string, or a function-designator
that takes a progress-bar instance and returns a string.")
  (total-steps nil :type (or null integer))
  (current-step 0 :type integer)
  (min-time progress-bar-min-time
            :type float
            :documentation "The minimum time interval between progress bar displays.")
  (min-change progress-bar-min-change
              :type integer
              :documentation "The minimum percentage change between progress bar displays.")
  (data nil :documentation "Extra data stored in the progress-bar instance for convenience.
Often contains current element being processed.")
  (displayed-time 0.0 :type float
                  :documentation "Time of last display.")
  (displayed-percentage 0 :type integer
                        :documentation "Last percentage displayed."))

(defun progress-bar-completed-p (progress-bar)
  "Return T if PROGRESS-BAR has completed."
  (with-slots (current-step total-steps) progress-bar
    (= current-step total-steps)))

(defun progress-bar-update (progress-bar &rest args)
  "Update PROGRESS-BAR and display it.
ARGS is a property-list of slot-name and value.

Example:
(progress-bar-update pg 'current-step 2 'data 'foo)"
  (cl-loop for (slot value) on args by 'cddr
           do (setf (slot-value progress-bar slot) value))
  (progress-bar--display progress-bar))

(defun progress-bar-incf (progress-bar &optional increment update)
  "Increment step in PROGRESS-BAR."
  (cl-incf (progress-bar-current-step progress-bar) (or increment 1))
  (when update
    (progress-bar--display progress-bar)))

(defvar progress-bar--message (symbol-function 'message))

(defun progress-bar-percentage (progress-bar)
  "Current completion percentage of PROGRESS-BAR."
  (with-slots (current-step total-steps) progress-bar
    (truncate (* (/ current-step (float total-steps)) 100))))

(defun progress-bar--display (progress-bar)
  "Display PROGRESS-BAR in echo-area."
  (with-slots (total-steps min-time displayed-time min-change displayed-percentage)
      progress-bar
    (let ((now (float-time))
          (percentage (progress-bar-percentage progress-bar)))
      (when (or (progress-bar-completed-p progress-bar)
                (and (>= total-steps progress-bar-min-steps)
                     (>= now (+ displayed-time min-time))
                     (>= percentage (+ displayed-percentage min-change))))
        ;; Disable logging, and don't allow resize of echo area while displaying the progress bar
        (let ((message-log-max nil)
              (resize-mini-windows nil))
          (funcall progress-bar--message (progress-bar--display-string progress-bar))
          (setf displayed-time now
                displayed-percentage percentage))))))

(when nil
  (dotimes-with-progress-bar (x 50)
      (sit-for 0.1)))

(defun progress-bar--display-string (progress-bar)
  "String representation of the PROGRESS-BAR."
  (with-slots (current-step total-steps status-message)
      progress-bar
    (let ((msg
           (cl-etypecase status-message
             (null nil)
             ((or symbol function)
              (funcall status-message progress-bar))
             (string status-message))))
      (if (zerop total-steps)
          (or msg "")
        (let* ((completed (/ current-step (float total-steps)))
               (chars (truncate (* completed progress-bar-width))))
          (with-output-to-string
            (dotimes (_c chars)
              (princ (string progress-bar-char)))
            (dotimes (_c (- progress-bar-width chars))
              (princ (string progress-bar-background-char)))
            (when msg
              (princ " ")
              (princ msg))
            (princ (format progress-bar-format-string current-step total-steps (truncate (* completed 100))))))))))

(defun call-with-progress-bar (progress-bar func)
  "Call FUNC using PROGRESS-BAR.
Sets up special treatment for calls to MESSAGE that may occur when
evaluating FUNC, so that messages are displayed together with the progress bar."
  (if (< (progress-bar-total-steps progress-bar) progress-bar-min-steps)
      ;; If total-steps are not enough, then do nothing with the progress-bar
      (funcall func progress-bar)
    ;; Replace the implementation of `message' temporarily, so that
    ;; messages sent by FUNC are shown together with the progress bar.
    (let ((emacs-message (symbol-function 'message)))
      (cl-flet ((pb-message (msg &rest args)
                  ;; This is only for logging. Can we log the message
                  ;; without calling `message' ?
                  ;;(apply emacs-message msg args)
                  (let ((message-log-max nil)
                        (resize-mini-windows nil))
                    (if (null msg)
                        (funcall emacs-message (progress-bar--display-string progress-bar))
                      (apply emacs-message (concat (progress-bar--display-string progress-bar) " | " msg) args)))))
        (cl-letf (((symbol-function #'message) #'pb-message))
          (funcall func progress-bar))))))

(defmacro with-progress-bar (spec &rest body)
  "Create a PROGRESS-BAR binding SPEC in BODY scope.
SPEC has either the form (VAR PROGRESS-BAR-INSTANCE) or (VAR &rest INITARGS), with
INITARGS used for creating a `progress-bar'.
This macros sets up special treatment for calls to MESSAGE that may ocurr in BODY,
so that messages are displayed together with the progress bar."
  (declare (indent 2))
  (cl-destructuring-bind (var &rest initargs) spec
    (if (= (length initargs) 1)
        `(let ((,var ,(car initargs)))
           (call-with-progress-bar ,var (lambda (,var) ,@body)))
      `(let ((,var (make-progress-bar ,@initargs)))
         (call-with-progress-bar ,var (lambda (,var) ,@body))))))

(when nil
  (with-progress-bar (pg :status-message "Hello" :total-steps 10)
      (cl-loop for x from 1 to 10
               do
               (progress-bar-incf pg)
               (message "This is a message!!: %s" x)
               (sit-for 0.3)
               )))

(when nil
  (let ((progress-bar-min-steps 5))
    (with-progress-bar (pg :status-message "Hello" :total-steps 4)
        (cl-loop for x from 1 to 4
                 do
                 (progress-bar-incf pg 1 t)
                 (message "This is a message!!: %s" x)
                 (sit-for 0.3)
                 ))))

;; Utilities

(defun mapc-with-progress-bar (func sequence &rest args)
  "Like `mapc' but using a progress-bar."
  (let ((progress-bar (apply #'make-progress-bar
                             :total-steps (length sequence)
                             :current-step 0
                             args)))
    (with-progress-bar (progress-bar progress-bar)
        (dolist (x sequence)
          (setf (progress-bar-data progress-bar) x)
          (funcall func x)
          (cl-incf (progress-bar-current-step progress-bar))
          (progress-bar--display progress-bar))
      (setf (progress-bar-data progress-bar) nil)
      (progress-bar--display progress-bar))))

(when nil
  (mapc-with-progress-bar
   (lambda (x)
     (message (format "Processing %s .." x))
     (sit-for 0.3))
   (cl-loop for i from 1 to 10 collect i)))

(when nil
  (mapc-with-progress-bar
   (lambda (x) (sit-for 0.3))
   (cl-loop for i from 1 to 10 collect i)
   :status-message (lambda (pg)
                     (if (progress-bar-completed-p pg)
                         "All elems processed"
                       (format "Processing %s .." (progress-bar-data pg))))))

(defmacro dolist-with-progress-bar (spec &rest body)
  "Like DOLIST but displaying a progress-bar as items in the list are processed.
ARGS are arguments for `make-progress-bar'.

\(fn (VAR LIST ARGS...) BODY...)

Example:

\(dolist-with-progress-bar
   (x (cl-loop for i from 1 to 30 collect i)
      :status-message \"Working ...\")
   (sit-for 0.3))"
  (declare (indent 2))
  (cl-destructuring-bind (var list &rest args) spec
    (let ((progress-bar (gensym "progress-bar-")))
      `(let ((,progress-bar (make-progress-bar
                             :total-steps (length ,list)
                             :current-step 0
                             ,@args)))
         (with-progress-bar (,progress-bar ,progress-bar)
             (dolist (,var ,list)
               (setf (progress-bar-data ,progress-bar) ,var)
               ,@body
               (cl-incf (progress-bar-current-step ,progress-bar))
               (progress-bar--display ,progress-bar))
           (setf (progress-bar-data ,progress-bar) nil)
           (progress-bar--display ,progress-bar)
           )))))

(when nil
  (dolist-with-progress-bar (x (cl-loop for i from 1 to 10 collect i)
                               :status-message "Working ...")
      (sit-for 0.3)))

(when nil
  (dolist-with-progress-bar (x (cl-loop for i from 1 to 10 collect i)
                               :status-message "Working ...")
      (message "Hello %s" x)
    (sit-for 0.3)))

(defmacro dotimes-with-progress-bar (spec &rest body)
  "Like `dotimes' but with a progress bar."
  (declare (indent 2))
  (let ((progress-bar (gensym "progress-bar-")))
    (cl-destructuring-bind (var times &rest args) spec
      `(let ((,progress-bar (make-progress-bar
                             :total-steps ,times
                             :current-step 0
                             ,@args)))
         (with-progress-bar (,progress-bar ,progress-bar)
             (dotimes (,var ,times)
               (setf (progress-bar-data ,progress-bar) ,var)
               ,@body
               (cl-incf (progress-bar-current-step ,progress-bar))
               (progress-bar--display ,progress-bar))
           (setf (progress-bar-data ,progress-bar) nil)
           (progress-bar--display ,progress-bar))))))

(when nil
  (dotimes-with-progress-bar (x 10 :status-message "Working ...")
      (sit-for 0.3)))

(when nil
  (dotimes-with-progress-bar (x 10 :status-message "Working ...")
      (message "Hello %s" x)
    (sit-for 0.3)))

(provide 'progress-bar)

;;; progress-bar.el ends here
