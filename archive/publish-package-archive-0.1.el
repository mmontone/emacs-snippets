;;; publish-package-archive.el --- Generate a Markdown file with the description of a package archive   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marian

;; Author: Marian <marianomontone@gmail.com>
;; Version: 0.1
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Generate a Markdown file with the description of the packages of a package archive.

;;; Code:

(defun publish-package-archive (archive-contents-file output-file &optional _format)
  "Publish a description of the packages of ARCHIVE-CONTENTS-FILE to OUTPUT-FILE.
FORMAT can be :markdown for now."
  (let* ((file-contents (with-temp-buffer
                          (insert-file-contents archive-contents-file)
                          (buffer-string)))
         (archive (cdar (read-from-string file-contents))))
    (with-temp-buffer
      (dolist (package-entry (cdr archive))
        (let* ((package-name (car package-entry))
               (package-version (aref (cdr package-entry) 0))
               (package-summary (aref (cdr package-entry) 2))
               (package-props (aref (cdr package-entry) 4))
               (package-authors (cdr (cl-find :authors package-props :key #'car)))
               ;;(package-maintainer (cdr (cl-find :maintainer package-props :key #'car)))
               (package-keywords (cdr (cl-find :keywords package-props :key #'car))))
          (insert "### " (prin1-to-string package-name)) (newline 2)
          (insert package-summary) (newline 2)
          (insert "- **Version:** " (mapconcat #'prin1-to-string package-version "."))
          (newline)
          (insert "- **Author**: " (mapconcat (lambda (author)
                                                (concat (car author) " <" (cdr author) ">"))
                                              package-authors))
          (newline)
          (insert "- **Keywords:** " (mapconcat #'identity package-keywords ", "))
          (newline)
          (newline)))
      (write-file output-file))))

(provide 'publish-package-archive)
;;; publish-package-archive.el ends here
