Commands for accessing index.scheme.org

You may want to add key bindings to scheme-mode:

(add-hook 'scheme-mode-hook
         (lambda ()
           (local-set-key (kbd "C-c C-d") 'scmindex-describe-symbol)))
