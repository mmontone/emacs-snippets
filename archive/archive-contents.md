### publish-archive-contents

Generate a Markdown file with descriptions of the package of an archive-contents

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools

### cider-customizations

Customizations for CIDER (Clojure mode)

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience, tools

### move-file

Command for moving a file to a new location

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience

### package-list-archive

List the packages of an archive

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience

### package-list-topic

List packages for a given topic

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience

### preview-package-source

Preview package source before installing

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience, tools

### ert-run-test-at-point

Run ERT test at point

- **Version:** 0.3
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, testing, ert, convenience

### indent-buffer

Command for indenting the whole buffer

- **Version:** 0.1
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience

### apropos-package

Apropos command for searching Emacs packages

- **Version:** 0.3
- **Author**: Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, package, utilities, snippet

