;;; ert-run-test-at-point.el --- Run ERT test at point  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Marian <marianomontone@gmail.com>
;; Version: 0.1
;; Keywords: tools, testing, ert

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Run ERT test at point

;;; Code:

(defun ert--defun-at-point ()
  "Return the text of the defun at point."
  (apply #'buffer-substring-no-properties
         (ert--region-for-defun-at-point)))

(defun ert--region-for-defun-at-point ()
  "Return the start and end position of defun at point."
  (save-excursion
    (save-match-data
      (end-of-defun)
      (let ((end (point)))
        (beginning-of-defun)
        (list (point) end)))))

(defun ert-run-test-at-point ()
  "Run the ert test at point."
  (interactive)
  (let* ((test-form (read-from-string (ert--defun-at-point)))
         (test-name (cadar test-form)))
    (ert test-name)))

(provide 'ert-run-test-at-point)
;;; ert-run-test-at-point.el ends here
