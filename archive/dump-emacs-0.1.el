;;; dump-emacs.el --- Command for dumping an Emacs core  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Command for dumping an Emacs core.
;; Makes Emacs start much faster.

;; Usage:
;;
;; Load this package from Emacs init file.
;; Start Emacs and run `dump-emacs' command.
;; Start Emacs from the dumped core with the following command:
;; emacs -q --dump-file ~/.emacs.d/emacs.dump -l ~/.emacs.d/load-path.el

;;; Code:

(defun dump-load-path ()
  "Dump the load path.
Dumped Emacs has trouble recovering the load-path.
This function serializes the load-path to a file that should be loaded
after a dumped Emacs starts."
  (with-temp-buffer
    (insert (prin1-to-string `(setq load-path ',load-path)))
    (fill-region (point-min) (point-max))
    (write-file "~/.emacs.d/load-path.el")))

(defun dump-emacs ()
  "Dump current Emacs config."
  (interactive)
  (shell-command "emacs --batch -l ~/.emacs -eval '(dump-load-path)' -eval '(dump-emacs-portable \"~/.emacs.d/emacs.dump\")'"))

(provide 'dump-emacs)

;;; dump-emacs.el ends here
