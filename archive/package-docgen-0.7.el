;;; package-docgen.el --- Documentation generator for Emacs simple packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, docs
;; Version: 0.7

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Documentation generator for Emacs simple packages.
;;
;; Simple packages are packages that are contained in a single source file:
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Simple-Packages.html
;;
;; This package automatically generates documentation for them.
;; Extracts structured information about it, like description, version and
;; its different types of definitions exposed and outputs a file with documentation.
;; The definitions considered exposed are those that don't have double dashes in their name.
;; Documentation is generated for customisation groups and variables, functions, variables and faces.
;; Currently only Markdown format is implemented.
;;
;; Usage:
;; M-x package-docgen-markdown with package (file) and output file name.

;;; Code:

(require 'cl-lib)
(require 'find-func)
(require 'package)
(require 'subr-x)
(require 'texinfo)

(defun package-doc--escape-string (string escape-character escape-characters)
  "Escape STRING using ESCAPE-CHARACTER.  ESCAPE-CHARACTERS is the list of characters to escape."
  (let ((chars '()))
    (seq-doseq (char string)
      (when (member char escape-characters)
        (push escape-character chars))
      (push char chars))
    (apply #'string (nreverse chars))))

(defvar markdown--escape-characters (list ?\\ ?` ?* ?_ ?{ ?} ?\( ?\) ?# ?+ ?- ?. ?!))

(defun markdown-escape-string (string)
  "Escape markdown STRING."
  (package-doc--escape-string string ?\\ markdown--escape-characters))

;; (markdown-escape-string "**fooo**")

(defun texinfo-escape-string (string)
  "Escape TexInfo STRING."
  (package-doc--escape-string string ?@ (list ?{ ?} ?@)))

(defun package-docgen--version-string (version)
  "Print VERSION to a string."
  (string-join (cl-mapcar #'prin1-to-string version) "."))

(defun package-docgen--read-package-info (package-file)
  "Return a `package-desc' describing the package in PACKAGE-FILE.

If the file does not contain a conforming package, signal an error."
  (with-temp-buffer
    (insert-file-contents package-file)
    (package-buffer-info)))

(defgroup package-docgen nil
  "package-docgen settings.")

(defcustom package-docgen-internal-definition-patterns '("--")
  "Patterns that indicates a package definition should be considered internal."
  :type '(repeat string)
  :group 'package-docgen)

(defun package-docgen--external-symbol-p (symbol)
  "Determines if SYMBOL to be considered \"external\"."
  (not (cl-some (lambda (pattern) (cl-search pattern (symbol-name symbol)))
                package-docgen-internal-definition-patterns)))

;; (describe-symbol 'inspector-action-face)
;; (documentation-property 'inspector-action-face 'face-documentation)
;; (documentation 'inspector-inspect)
;; (documentation 'inspector-inspect-last-sexp)
;; (documentation-property 'inspector-truncation-limit 'variable-documentation)
;; (symbol-plist 'inspector-truncation-limit)
;; (symbol-plist 'inspector-action-face)

(defun package-docgen--symbol-package (symbol)
  "Ad-hoc function for determining the package of SYMBOL."

  )

;; (find-library-name "inspector")
;; (find-function-library 'inspector-inspect)
;; (symbol-file 'inspector-truncation-limit 'defvar)
;; (symbol-file 'inspector-inspect 'defun)
;; (symbol-file 'inspector-inspect-last-sexp 'defun)
;; (symbol-file 'inspector-action-face 'defface)
;; (commandp 'inspector-inspect-last-sexp)

(defun package-docgen--read-all-from-stream (stream callback)
  (condition-case nil
      (while t
        (let ((sexp (read stream)))
          (funcall callback sexp)))
    (end-of-file)))

(defun read-from-file (filename callback)
  "Read sexps in FILENAME and calling CALLBACK."
  (with-temp-buffer
    (insert-file-contents filename)
    (goto-char 0)
    (package-docgen--read-all-from-stream (current-buffer) callback)))

;; (read-from-file (find-library-name "inspector") #'debug)
;; (read-from-file (find-library-name "inspector") #'print)

(defun package-docgen--package-definitions (path-or-package-name)
  "List definitions for PATH-OR-PACKAGE-NAME."
  (let ((package-file (if (file-exists-p path-or-package-name)
                          path-or-package-name
                        (find-library-name path-or-package-name)))
        (defs (list (cons 'defun ())
                    (cons 'defvar ())
                    (cons 'defcustom ())
                    (cons 'defface ())
                    (cons 'defgroup ()))))
    (read-from-file package-file
                    (lambda (sexp)
                      (cl-case (car sexp)
                        (defun (push (cons (cadr sexp) sexp)
                                     (alist-get 'defun defs)))
                        (defvar (push (cons (cadr sexp) sexp)
                                      (alist-get 'defvar defs)))
                        (defcustom (push (cons (cadr sexp) sexp)
                                         (alist-get 'defcustom defs)))
                        (defgroup (push (cons (cadr sexp) sexp)
                                        (alist-get 'defgroup defs)))
                        (defface (push (cons (cadr sexp) sexp)
                                       (alist-get 'defface defs))))))
    defs))

(defun package-docgen-markdown (path-or-package-name filename)
  "Write documentation for PATH-OR-PACKAGE-NAME in FILENAME.
If PATH-OR-PACKAGE-NAME is a path, then it expects a file
with an Emacs Lisp package file."
  (interactive "fPackage or filename: \nFWrite to: ")

  (cl-labels ((insert-doc (doc)
                ;;(insert "```") (newline)
                (insert "    ")
                (insert-rectangle (split-string (markdown-escape-string doc) "\n"))
                ;; (newline) (insert "```")
                )
              (insert-definition-section (section defs)
                (insert "## " (cdr section))
                (newline 2)
                (dolist (def defs)
                  (cl-case (car section)
                    (defun
                        (insert "- **" (prin1-to-string (car def)) "** ")
                        (cl-destructuring-bind (_ _fname fargs &rest fbody) (cdr def)
                          (insert (if (null fargs) "()" (prin1-to-string fargs)))
                          (when (stringp (car fbody))
                            (newline 2)
                            (insert-doc (car fbody)))))
                    (defvar
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _varname &optional initvalue docstring) (cdr def)
                        (insert (format "`%s`" initvalue))
                        (when docstring
                          (newline 2)
                          (insert-doc docstring))))
                    (defgroup
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _groupname _members doc &rest args) (cdr def)
                        (newline 2)
                        (insert-doc doc)))
                    (defcustom
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _varname standard doc &rest args) (cdr def)
                        (insert (format "`%s`" standard))
                        (insert (format " (%s)" (cadr (cl-getf args :type))))
                        (newline 2)
                        (insert-doc doc)))
                    (defface
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _name _spec doc &rest args) (cdr def)
                        (newline 2)
                        (insert-doc doc)))
                    )
                  (newline 2))))
    (let* ((package-name (if (file-exists-p path-or-package-name)
                             (file-name-base path-or-package-name)
                           path-or-package-name))
           (package-file (if (file-exists-p path-or-package-name)
                             path-or-package-name
                           (find-library-name path-or-package-name)))
           (pck-desc (package-docgen--read-package-info package-file))
           (extras (package-desc-extras pck-desc))
           (defs (package-docgen--package-definitions path-or-package-name))
           (sections '((defgroup . "Customisation groups")
                       (defcustom . "Customisations")
                       (defun . "Functions")
                       (defvar . "Variables")
                       (defface . "Faces"))))
      (with-temp-buffer
        (insert "# " package-name)
        (newline 2)
        (insert (package-desc-summary pck-desc))
        (newline 2)

        (when-let ((version (package-desc-version pck-desc)))
          (insert "- Version: ")
          (insert (package-docgen--version-string version))
          (newline))

        (when-let ((reqs (package-desc-reqs pck-desc)))
          (insert "- Requirements: ")
          (let ((req (car reqs)))
            (insert (prin1-to-string (car req)))
            (insert " v")
            (insert (package-docgen--version-string (cadr req))))
          (dolist (req (cdr reqs))
            (insert ", ")
            (insert (prin1-to-string (car req)))
            (insert " v")
            (insert (package-docgen--version-string (cadr req))))
          (newline))
        (when-let ((authors (cdr (assoc :authors extras))))
          (insert "- Authors: ")
          (let ((author (car authors)))
            (insert (car author))
            (insert " <")
            (insert (cdr author))
            (insert ">"))
          (dolist (author (cdr authors))
            (insert ", ")
            (insert (car author))
            (insert " <")
            (insert (cdr author))
            (insert ">"))
          (newline))

        (when-let ((maintainer (cdr (assoc :maintainer extras))))
          (insert "- Maintainer: ")
          (insert (car maintainer))
          (insert " <")
          (insert (cdr maintainer))
          (insert ">")
          (newline))

        (newline 2)
        (setf (package-desc-dir pck-desc)
              (file-name-directory package-file))
        (insert (package--get-description pck-desc))
        (newline 2)

        (dolist (section sections)
          (let ((section-defs (alist-get (car section) defs)))
            (setq section-defs (cl-remove-if-not (lambda (def)
                                                   (package-docgen--external-symbol-p (car def)))
                                                 section-defs))
            (when section-defs
              (insert-definition-section section section-defs))))
        (write-file filename)))))

;; (package-docgen--package-definitions "inspector")
;; (package-docgen-markdown "inspector" "/home/marian/src/inspector.md")
;; (package-docgen--package-definitions "package-docgen")
;; (package-docgen-markdown "package-docgen" "/home/marian/src/package-docgen.md")

(defun package-docgen-texinfo (path-or-package-name filename)
  "Write documentation for PATH-OR-PACKAGE-NAME in FILENAME.
If PATH-OR-PACKAGE-NAME is a path, then it expects a file
with an Emacs Lisp package file."
  (interactive "fPackage or filename: \nFWrite to: ")

  (cl-labels ((insert-doc (doc)
                (insert "    ")
                (insert-rectangle (split-string (texinfo-escape-string doc) "\n")))
              (insert-definition-section (section defs)
                (insert "@node " (cdr section) "\n")
                (insert "@chapter " (cdr section) "\n")
                (dolist (def defs)
                  (cl-case (car section)
                    (defun
                        (cl-destructuring-bind (_ fname fargs &rest fbody) (cdr def)
                          (insert "@defun " (prin1-to-string fname) " " (if (null fargs) "()" (prin1-to-string fargs)) "\n")
                          (when (stringp (car fbody))
                            (newline 2)
                            (insert-doc (car fbody)))
                          (insert "\n@end defun\n")))
                    (defvar
                      (cl-destructuring-bind (_ varname &optional initvalue docstring) (cdr def)
                        (insert "@defvar " (prin1-to-string varname) "\n")
                        (when docstring
                          (newline 2)
                          (insert-doc docstring))
                        (insert "\n@end defvar\n")))
                    (defgroup
                      (cl-destructuring-bind (_ groupname _members doc &rest args) (cdr def)
                        ;;(insert "@defblock\n@defline CustomizationGroup " (prin1-to-string groupname) "\n")
                        (insert "@deftp {Customization Group} " (prin1-to-string groupname) "\n")
                        (insert-doc doc)
                        (insert "\n@end deftp")
                        ;;(insert "\n@end defblock\n")
                        ))
                    (defcustom
                      (cl-destructuring-bind (_ varname standard doc &rest args) (cdr def)
                        (insert "@defvr {User Option} " (prin1-to-string varname )"\n")
                        ;;(insert (format "`%s`" standard))
                        ;;(insert (format " (%s)" (cadr (cl-getf args :type))))
                        ;;(newline 2)
                        (insert-doc doc)
                        (insert "\n@end defvr\n")))
                    (defface
                      (cl-destructuring-bind (_ name _spec doc &rest args) (cdr def)
                        (insert "@deftp {Face} " (prin1-to-string name) "\n")
                        (newline 2)
                        (insert-doc doc)
                        (insert "\n@end deftp\n"))))
                  (newline 2))))
    (let* ((package-name (if (file-exists-p path-or-package-name)
                             (file-name-base path-or-package-name)
                           path-or-package-name))
           (package-file (if (file-exists-p path-or-package-name)
                             path-or-package-name
                           (find-library-name path-or-package-name)))
           (pck-desc (package-docgen--read-package-info package-file))
           (extras (package-desc-extras pck-desc))
           (defs (package-docgen--package-definitions path-or-package-name))
           (sections '((defgroup . "Customisation groups")
                       (defcustom . "Customisations")
                       (defun . "Functions")
                       (defvar . "Variables")
                       (defface . "Faces"))))
      (with-temp-buffer
        (insert "@setfilename " package-name "\n")
        (insert "@settitle " package-name "\n\n")
        (insert "@dircategory Emacs Lisp\n")
        (insert "@direntry\n")
        (insert "* " package-name ": " package-name "\n")
        (insert "@end direntry\n\n")
        (insert "@titlepage\n")
        (insert "@title " package-name "\n")
        (insert "@end titlepage\n")
        (insert "@contents\n")
        (insert "@node Top\n")
        (insert "@top " package-name "\n")
        (insert (package-desc-summary pck-desc))
        (newline)
        (when-let ((version (package-desc-version pck-desc)))
          (insert "- Version: ")
          (insert (package-docgen--version-string version))
          (newline))
        (when-let ((reqs (package-desc-reqs pck-desc)))
          (insert "- Requirements: ")
          (let ((req (car reqs)))
            (insert (prin1-to-string (car req)))
            (insert " v")
            (insert (package-docgen--version-string (cadr req))))
          (dolist (req (cdr reqs))
            (insert ", ")
            (insert (prin1-to-string (car req)))
            (insert " v")
            (insert (package-docgen--version-string (cadr req))))
          (newline))
        (when-let ((authors (cdr (assoc :authors extras))))
          (insert "- Authors: ")
          (let ((author (car authors)))
            (insert (car author))
            (insert " <")
            (insert (texinfo-escape-string (cdr author)))
            (insert ">"))
          (dolist (author (cdr authors))
            (insert ", ")
            (insert (car author))
            (insert " <")
            (insert (texinfo-escape-string (cdr author)))
            (insert ">"))
          (newline))

        (when-let ((maintainer (cdr (assoc :maintainer extras))))
          (insert "- Maintainer: ")
          (insert (car maintainer))
          (insert " <")
          (insert (texinfo-escape-string (cdr maintainer)))
          (insert ">")
          (newline))
        (newline 2)
        (setf (package-desc-dir pck-desc)
              (file-name-directory package-file))
        (insert (package--get-description pck-desc))
        (newline 2)

        (insert "@menu\n")
        (insert "* Customisation groups::\n")
        (insert "* Customisations::\n")
        (insert "* Variables::\n")
        (insert "* Functions::\n")
        (insert "* Faces::\n")
        (insert "* Index::\n")
        (insert "@end menu\n")

        (dolist (section sections)
          (let ((section-defs (alist-get (car section) defs)))
            (setq section-defs (cl-remove-if-not (lambda (def)
                                                   (package-docgen--external-symbol-p (car def)))
                                                 section-defs))
            (when section-defs
              (insert-definition-section section section-defs))))
        (newline)
        (insert "@node Index\n")
        (insert "@chapter Index\n")
        (insert "@menu\n")
        (insert "* Variable Index::\n")
        (insert "* Function Index::\n")
        (insert "@end menu\n")
        (insert "@node Variable Index\n")
        (insert "@section Variable Index\n")
        (insert "@printindex vr\n")
        (insert "@node Function Index\n")
        (insert "@section Function Index\n")
        (insert "@printindex fn\n")
        (insert "@bye")
        (write-file filename)))))

;; (package-docgen-texinfo "inspector" "/home/marian/src/inspector.texi")

(defun package-docgen-info (path-or-package-name)
  "Generate TeXinfo documentation for PATH-OR-PACKAGE-NAME, build an Info and display it in an Emacs Info buffer."
  (interactive "fPackage or filename: ")
  (let ((file (make-temp-file "package-docgen-" nil ".texi")))
    (package-docgen-texinfo path-or-package-name file)
    (find-file file)
    (makeinfo-buffer)))

(defun package-docgen-buffer-file-info ()
  "Generate TeXinfo documentation for current buffer, build and Info and display it."
  (interactive)
  (package-docgen-info (buffer-file-name)))

(provide 'package-docgen)

;;; package-docgen.el ends here
