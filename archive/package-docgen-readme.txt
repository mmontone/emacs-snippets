Documentation generator for Emacs simple packages.

Simple packages are packages that are contained in a single source file:
https://www.gnu.org/software/emacs/manual/html_node/elisp/Simple-Packages.html

This package automatically generates documentation for them.
Extracts structured information about it, like description, version and
its different types of definitions exposed and outputs a file with documentation.
The definitions considered exposed are those that don't have double dashes in their name.
Documentation is generated for customisation groups and variables, functions, variables and faces.
Currently Markdown and Texinfo formats are implemented.

Usage:
M-x package-docgen-markdown with package (file) and output file name.
M-x package-docgen-texinfo with package (file) and output file name.
M-x package-docgen-info with package (file) to generate Texinfo, compile to Info and visualize using Info reader.

Open any Emacs simple package file, and do: M-x package-docgen-info RET RET to visualize its documentation in Info reader.