;;; emacs-info.el --- Display info about current Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: convenience, tools
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Display info about current Emacs

;;; Code:

(require 'cl-lib)

(defun emacs-info ()
  "Show info about current Emacs."
  (interactive)
  (cl-flet ((yes-or-no (boolean) (if boolean "yes" "no")))
    (let ((buffer (get-buffer-create "*emacs-info*")))
      (with-current-buffer buffer
        (erase-buffer)
        (insert "Version: ")
        (insert (emacs-version))
        (newline)
        (insert "Features: ")
        (insert system-configuration-features)
        (newline)
        (insert "Native compilation: ")
        (insert (yes-or-no (cl-search "NATIVE_COMP" system-configuration-features)))
        (newline)
        (insert "SVG: ")
        (insert (yes-or-no (image-type-available-p 'svg))))
      (display-buffer buffer))))

(provide 'emacs-info)

;;; emacs-info.el ends here
