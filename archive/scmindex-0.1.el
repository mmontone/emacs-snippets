;;; scmindex.el --- Commands for accessing index.scheme.org  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Marian <marianomontone@gmail.com>
;; Keywords: convenience, tools
;; Version: 0.1
;; Package-Requires: ((emacs "26") (request "0.3.3"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Commands for accessing index.scheme.org
;;
;; You may want to add key bindings to scheme-mode:
;;
;; (add-hook 'scheme-mode-hook
;;          (lambda ()
;;            (local-set-key (kbd "C-c C-d") 'scmindex-describe-symbol)))
;;
;;; Code:

(require 'request)
(require 'thingatpt)
(require 'json)
(require 'at)

(defgroup scmindex nil
  "Scheme index settings.")

(defcustom scmindex-filterset "r7rs_all"
  "Scheme index filter set."
  :type '(choice (const "r7rs_all")
                 (const "r7rs_small")
                 (const "r5rs")
                 (const "r6rs")
                 (const "r6rs_all"))
  :group 'scmindex)

(defcustom scmindex-url "https://index.scheme.org"
  "Scheme index url."
  :type 'string
  :group 'scmindex)

(defun scmindex--insert-item (item)
  (when-let ((name  (at item "name")))
    (insert name)
    (newline 2)
    (when-let ((desc (at item "description")))
      (insert desc))))

(defun scmindex-describe-symbol (symbol)
  "Describe Scheme SYMBOL at point."

  (interactive (list (symbol-at-point)))

  (let* ((response (request (concat scmindex-url "/rest/filterset/" scmindex-filterset "/search?query=" (symbol-name symbol) "&facet=false&rows=1") :sync t))
         (data (request-response-data response))
         (json (json-parse-string data))
         (items (at json "items")))
    (with-current-buffer (get-buffer-create (format "*scmindex: %s*" (symbol-name symbol) ))
      (cl-map 'list (lambda (item)
                      (scmindex--insert-item item)
                      (newline 2))
              items)
      (goto-char 0)
      (setq buffer-read-only t)
      (pop-to-buffer (current-buffer)))))

(defun scmindex-apropos (query)
  "Show all meaningful Scheme symbols whose names match QUERY."

  (interactive "sScheme index apropos: ")

  (let* ((response (request (concat scmindex-url "/rest/filterset/" scmindex-filterset "/search?query=" query "&facet=false") :sync t))
         (data (request-response-data response))
         (json (json-parse-string data))
         (items (at json "items")))
    (with-current-buffer (get-buffer-create (format "*scmindex: %s*" query ))
      (cl-map 'list (lambda (item)
                      (when-let ((name (at item "name")))
                        (insert-button name
                                       'face 'apropos-symbol
                                       'action (lambda (_btn)
                                                 (scmindex-describe-symbol (intern name))))
                        (newline)
                        (insert "  ")
                        (insert (cl-subseq (at item "description")
                                           0
                                           (cl-position ?. (at item "description"))))
                        (newline 2)))
              items)
      (goto-char 0)
      (setq buffer-read-only t)
      (pop-to-buffer (current-buffer)))))

(provide 'scmindex)

;;; scmindex.el ends here
