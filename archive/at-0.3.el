;;; at.el --- Generic accessing library              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: convenience
;; Package-Version: 0.3
;; Package-Requires: ((emacs "26") (s "0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Generic accessing library.

;;; Code:

(require 'cl-lib)
(require 'eieio)
(require 's)

(defun at--proper-list-p (val)
  "Is VAL a proper list?"
  (if (fboundp 'format-proper-list-p)
      ;; Emacs stable.
      (with-no-warnings (format-proper-list-p val))
    ;; Function was renamed in Emacs master:
    ;; http://git.savannah.gnu.org/cgit/emacs.git/commit/?id=2fde6275b69fd113e78243790bf112bbdd2fe2bf
    (with-no-warnings (proper-list-p val))))

(defun at--plistp (list)
  "Return T if LIST is a property list."
  (let ((expected t))
    (and (at--proper-list-p list)
         (cl-evenp (length list))
         (cl-every (lambda (x)
                     (setq expected (if (eql expected t) 'symbol t))
                     (cl-typep x expected))
                   list))))

(defun at--alistp (list)
  "Return T if LIST is an association list."
  (and (at--proper-list-p list)
       (cl-every (lambda (x) (consp x)) list)))

(defun at-1 (object key)
  (cl-typecase object
    (list (cond
           ((at--plistp object)
            (cl-getf object key))
           ((at--alistp object)
            (cdr (assoc key object)))
           ((numberp key)
            (nth key object))
           ((symbolp key)
            (funcall key object))
           (t
            (error "Cannot access: %s at: %a" object key))))
    (sequence (aref object key))
    (record (aref object key))
    (hash-table (gethash key object))
    (t
     (cond
      ((eieio-object-p object)
       (slot-value object key))
      ((symbolp key)
       (funcall key object))
      (t
       (error "Cannot access: %s at: %s" object key))))))

(defun at (object key &rest keys)
  (let ((obj (at-1 object key)))
    (dolist (key keys)
      (setq obj (at-1 obj key)))
    obj))

(defun at--subst-tree (substitution tree)
  (if (listp tree)
      (cl-mapcar (lambda (el)
                   (at--subst-tree substitution el))
                 tree)
    (funcall substitution tree)))

(defun at--subst-dot (tree)
  (at--subst-tree (lambda (el)
                    (if (and (symbolp el)
                             (cl-find ?. (symbol-name el)))
                        (let ((acc (cl-mapcar (lambda (x) (car (read-from-string x)))
                                              (s-split "\\." (symbol-name el)))))
                          (cl-list* 'at (car acc)
                                    (cl-mapcar (lambda (x)
                                                 (if (symbolp x)
                                                     (list 'quote x)
                                                   x))
                                               (cdr acc))))
                      el))
                  tree))

(defmacro at-with-syntax (&rest body)
  (at--subst-dot `(progn ,@body)))

(provide 'at)

;;; at.el ends here
