Command for dumping an Emacs core.
Makes Emacs start much faster.

Usage:

Load this package from Emacs init file.
Start Emacs and run `dump-emacs' command.
Start Emacs from the dumped core with the following command:
emacs -q --dump-file ~/.emacs.d/emacs.dump -l ~/.emacs.d/load-path.el