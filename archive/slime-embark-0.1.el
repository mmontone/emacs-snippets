;;; slime-embark.el --- Embark keymap for SLIME Lisp mode  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone(require 'slime) <marianomontone@gmail.com>
;; Keywords: tools
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Embark keymap for SLIME Lisp mode.

;;; Code:

(require 'slime)
(require 'embark)

(defvar slime-embark-keymap-alist
  '((file embark-file-map)
    (library embark-library-map)
    (environment-variables embark-file-map) ; they come up in file completion
    (url embark-url-map)
    (email embark-email-map)
    (buffer embark-buffer-map)
    (tab embark-tab-map)
    (expression slime-embark-expression-map)
    (identifier slime-embark-identifier-map)
    (defun slime-embark-defun-map)
    (symbol slime-embark-symbol-map)
                                        ;(face embark-face-map)
                                        ;(command embark-command-map)
    ;;(variable slime-embark-variable-map)
    (function slime-embark-function-map)
    (minor-mode embark-command-map)
    (unicode-name embark-unicode-name-map)
                                        ;(package embark-package-map)
    (bookmark embark-bookmark-map)
    (region embark-region-map)
    (sentence embark-sentence-map)
    (paragraph embark-paragraph-map)
    (kill-ring embark-kill-ring-map)
    (heading embark-heading-map)
    (t embark-general-map)))

(defvar-keymap slime-embark-identifier-map
  :doc "Keymap for Embark identifier actions."
  :parent embark-general-map
  "RET" #'xref-find-definitions
  "h" #'slime-describe-symbol
  "H" #'embark-toggle-highlight
  "d" #'slime-edit-definition
  "r" #'slime-edit-uses
  "a" #'slime-apropos
  "s" #'info-lookup-symbol
  "n" #'embark-next-symbol
  "p" #'embark-previous-symbol
  "'" #'expand-abbrev
  "$" #'ispell-word
  "o" #'occur)

(defvar-keymap slime-embark-defun-map
  :doc "Keymap for Embark defun actions."
  :parent embark-expression-map
  "RET" #'slime-pprint-eval-region
  "e" #'slime-eval-defun
  "c" #'slime-compile-defun
  ;;"l" #'elint-defun
  ;;"D" #'edebug-defun
  ;;"o" #'checkdoc-defun
  ;;"N" #'narrow-to-defun
  )

(defvar-keymap slime-embark-expression-map
  :doc "Keymap for Embark expression actions."
  :parent embark-general-map
  "RET" #'slime-pprint-eval-last-expression
  "e" #'slime-pprint-eval-last-expression
  ;;"<" #'embark-eval-replace
  "m" #'slime-macroexpand-all
  "TAB" #'indent-sexp
  "r" #'raise-sexp
  "t" #'transpose-sexps
  "k" #'kill-region
  "u" #'backward-up-list
  "n" #'forward-list
  "p" #'backward-list)

(defvar-keymap slime-embark-symbol-map
  :doc "Keymap for Embark symbol actions."
  :parent slime-embark-identifier-map
  "RET" #'slime-edit-definition
  "h" #'slime-describe-symbol
  ;;"s" #'embark-info-lookup-symbol
  "d" #'slime-edit-definition
  "e" #'slime-eval-last-expression
  "a" #'slime-apropos
  "\\" #'embark-history-remove)

(defvar-keymap slime-embark-function-map
  :doc "Keymap for Embark function actions."
  :parent slime-embark-symbol-map
  ;;"m" #'elp-instrument-function ;; m=measure
  ;;"M" 'elp-restore-function ;; quoted, not autoloaded
  ;;"k" #'slime-debug-on-entry ;; breaKpoint (running out of letters, really)
  ;;"K" #'cancel-debug-on-entry
  "t" #'slime-toggle-fancy-trace
  "T" #'slime-untrace-all)

(add-hook 'slime-mode-hook
          (lambda () (setq-local embark-keymap-alist slime-embark-keymap-alist)))

(provide 'slime-embark)
;;; slime-embark.el ends here
