;;; package-docgen.el --- Documentation generator for Emacs simple packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, docs
;; Version: 0.4

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Documentation generator for Emacs simple packages.
;;
;; Simple packages are packages that are contained in a single source file:
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Simple-Packages.html
;;
;; This package automatically generates documentation for them.
;; Extracts structured information about it, like description, version and
;; its different types of definitions exposed and outputs a file with documentation.
;; The definitions considered exposed are those that don't have double slashes in their name.
;; Documentation is generated for customisation groups and variables, functions, variables and faces.
;; Currently only Markdown format is implemented.
;;
;; Usage:
;; M-x package-docgen-markdown with package (file) and output file name.

;;; Code:

(require 'cl-lib)
(require 'find-func)
(require 'package)
(require 'subr)
(require 'subr-x)

(defvar markdown--escape-characters (list ?\\ ?` ?* ?_ ?{ ?} ?\( ?\) ?# ?+ ?- ?. ?!))

(defun markdown-escape-string (string)
  "Escape markdown STRING."
  (let ((chars '()))
    (seq-doseq (char string)
      (when (member char markdown--escape-characters)
        (push ?\\ chars))
      (push char chars))
    (apply #'string (nreverse chars))))

;; (markdown-escape-string "**fooo**")

(defun package-docgen--version-string (version)
  "Print VERSION to a string."
  (string-join (cl-mapcar #'prin1-to-string version) "."))

(defun package-docgen--read-package-info (package-file)
  "Return a `package-desc' describing the package in PACKAGE-FILE.

If the file does not contain a conforming package, signal an error."
  (with-temp-buffer
    (insert-file-contents package-file)
    (package-buffer-info)))

(defun package-docgen--external-symbol-p (symbol)
  "Determines if SYMBOL to be considered \"external\"."
  (not (cl-search "--" (symbol-name symbol))))

;; (describe-symbol 'inspector-action-face)
;; (documentation-property 'inspector-action-face 'face-documentation)
;; (documentation 'inspector-inspect)
;; (documentation 'inspector-inspect-last-sexp)
;; (documentation-property 'inspector-truncation-limit 'variable-documentation)
;; (symbol-plist 'inspector-truncation-limit)
;; (symbol-plist 'inspector-action-face)

(defun package-docgen--symbol-package (symbol)
  "Ad-hoc function for determining the package of SYMBOL."

  )

;; (find-library-name "inspector")
;; (find-function-library 'inspector-inspect)
;; (symbol-file 'inspector-truncation-limit 'defvar)
;; (symbol-file 'inspector-inspect 'defun)
;; (symbol-file 'inspector-inspect-last-sexp 'defun)
;; (symbol-file 'inspector-action-face 'defface)
;; (commandp 'inspector-inspect-last-sexp)

(defun read-from-file (filename callback)
  "Read sexps in FILENAME and calling CALLBACK."
  (condition-case nil
      (with-temp-buffer
        (insert-file-contents filename)
        (goto-char 0)
        (while t
          (let ((sexp (read (current-buffer))))
            (funcall callback sexp))))
    (end-of-file)))

;; (read-from-file (find-library-name "inspector") #'debug)
;; (read-from-file (find-library-name "inspector") #'print)

(defun package-docgen--package-definitions (path-or-package-name)
  "List definitions for PATH-OR-PACKAGE-NAME."
  (let ((package-file (if (file-exists-p path-or-package-name)
                          path-or-package-name
                        (find-library-name path-or-package-name)))
        (defs (list (cons 'defun ())
                    (cons 'defvar ())
                    (cons 'defcustom ())
                    (cons 'defface ())
                    (cons 'defgroup ()))))
    (read-from-file package-file
                    (lambda (sexp)
                      (cl-case (car sexp)
                        (defun (push (cons (cadr sexp) sexp)
                                     (alist-get 'defun defs)))
                        (defvar (push (cons (cadr sexp) sexp)
                                      (alist-get 'defvar defs)))
                        (defcustom (push (cons (cadr sexp) sexp)
                                         (alist-get 'defcustom defs)))
                        (defgroup (push (cons (cadr sexp) sexp)
                                        (alist-get 'defgroup defs)))
                        (defface (push (cons (cadr sexp) sexp)
                                       (alist-get 'defface defs))))))
    defs))

(defun package-docgen-markdown (path-or-package-name filename)
  "Write documentation for PATH-OR-PACKAGE-NAME in FILENAME.
If PATH-OR-PACKAGE-NAME is a path, then it expects a file
with an Emacs Lisp package file."
  (interactive "fPackage or filename: \nFWrite to: ")

  (cl-labels ((insert-doc (doc)
                ;;(insert "```") (newline)
                (insert "    ")
                (insert-rectangle (split-string (markdown-escape-string doc) "\n"))
                ;; (newline) (insert "```")
                )
              (insert-definition-section (section defs)
                (insert "## " (cdr section))
                (newline 2)
                (dolist (def defs)
                  (cl-case (car section)
                    (defun
                        (insert "- **" (prin1-to-string (car def)) "** ")
                        (cl-destructuring-bind (_ _fname fargs &rest fbody) (cdr def)
                          (insert (if (null fargs) "()" (prin1-to-string fargs)))
                          (when (stringp (car fbody))
                            (newline 2)
                            (insert-doc (car fbody)))))
                    (defvar
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _varname &optional initvalue docstring) (cdr def)
                        (insert (format "`%s`" initvalue))
                        (when docstring
                          (newline 2)
                          (insert-doc docstring))))
                    (defgroup
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _groupname _members doc &rest args) (cdr def)
                        (newline 2)
                        (insert-doc doc)))
                    (defcustom
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _varname standard doc &rest args) (cdr def)
                        (insert (format "`%s`" standard))
                        (insert (format " (%s)" (cadr (cl-getf args :type))))
                        (newline 2)
                        (insert-doc doc)))
                    (defface
                      (insert "- **" (prin1-to-string (car def)) "** ")
                      (cl-destructuring-bind (_ _name _spec doc &rest args) (cdr def)
                        (newline 2)
                        (insert-doc doc)))
                    )
                  (newline 2))))
    (let* ((package-name (if (file-exists-p path-or-package-name)
                             (file-name-base path-or-package-name)
                           path-or-package-name))
           (package-file (if (file-exists-p path-or-package-name)
                             path-or-package-name
                           (find-library-name path-or-package-name)))
           (pck-desc (package-docgen--read-package-info package-file))
           (extras (package-desc-extras pck-desc))
           (defs (package-docgen--package-definitions path-or-package-name))
           (sections '((defgroup . "Customisation groups")
                       (defcustom . "Customisations")
                       (defun . "Functions")
                       (defvar . "Variables")
                       (defface . "Faces"))))
      (with-temp-buffer
        (insert "# " package-name)
        (newline 2)
        (insert (package-desc-summary pck-desc))
        (newline 2)

        (when-let ((version (package-desc-version pck-desc)))
          (insert "- Version: ")
          (insert (package-docgen--version-string version))
          (newline))

        (when-let ((reqs (package-desc-reqs pck-desc)))
          (insert "- Requirements: ")
          (let ((req (car reqs)))
            (insert (prin1-to-string (car req)))
            (insert " v")
            (insert (package-docgen--version-string (cadr req))))
          (dolist (req (cdr reqs))
            (insert ", ")
            (insert (prin1-to-string (car req)))
            (insert " v")
            (insert (package-docgen--version-string (cadr req))))
          (newline))

        (when-let ((authors (at extras :authors)))
          (insert "- Authors: ")
          (let ((author (car authors)))
            (insert (car author))
            (insert " <")
            (insert (cdr author))
            (insert ">"))
          (dolist (author (cdr authors))
            (insert ", ")
            (insert (car author))
            (insert " <")
            (insert (cdr author))
            (insert ">"))
          (newline))

        (when-let ((maintainer (at extras :maintainer)))
          (insert "- Maintainer: ")
          (insert (car maintainer))
          (insert " <")
          (insert (cdr maintainer))
          (insert ">")
          (newline))

        (newline 2)
        (setf (package-desc-dir pck-desc)
              (file-name-directory package-file))
        (insert (package--get-description pck-desc))
        (newline 2)
        
        (dolist (section sections)
          (let ((section-defs (alist-get (car section) defs)))
            (setq section-defs (cl-remove-if-not (lambda (def)
                                                   (package-docgen--external-symbol-p (car def)))
                                                 section-defs))
            (when section-defs
              (insert-definition-section section section-defs))))
        (write-file filename)))))

;; (package-docgen--package-definitions "inspector")
;; (package-docgen-markdown "inspector" "/home/marian/src/inspector.md")
;; (package-docgen--package-definitions "package-docgen")
;; (package-docgen-markdown "package-docgen" "/home/marian/src/package-docgen.md")

(provide 'package-docgen)

;;; package-docgen.el ends here
