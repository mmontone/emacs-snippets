A buffer for displaying logs.

![Log and inspector](./log-buffer.png)

Usage:

Use (log-buffer-log level message [details] options...)

To log to different buffers, bind `log-buffer-name'. For example:

   (let ((log-buffer-name "*my-log*"))
      (log-buffer-log 'info "Hello"))

Create a logger for your application. To do that, pass a list
with application name plus log-level to log-buffer-log:

   (log-buffer-log '(my-app info) "Hello")

Loggers are created disabled by default, so you'll need to enable them
via log-buffer-enable-logger for the log buffer to be created.

To see the log buffer in action, evaluate: M-x log-buffer-test