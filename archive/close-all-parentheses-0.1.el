;;; close-all-parentheses.el --- Command for closing all parenthesis at once  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Sanel Z.

;; Author: Sanel Z. <sanelz@gmail.com>
;; Version: 0.1

;;; Commentary:

;; Command for closing all parenthesis at once.
;; Code extracted from: https://acidwords.com/posts/2017-10-19-closing-all-parentheses-at-once.html

;;; Code:

(require 'cl-macs)
(require 'cc-cmds)

(defun close-all-parentheses-simple ()
  "Simple implementation of command for closing all parentheses at once.

Doesn't work for C like languages.  Use `close-all-parentheses' for those."
  (interactive)
  (let ((closing nil))
    (save-excursion
      (while (condition-case nil
                 (progn
                   (backward-up-list)
                   (let ((syntax (syntax-after (point))))
                     (cl-case (car syntax)
                       ((4) (setq closing (cons (cdr syntax) closing)))
                       ((7 8) (setq closing (cons (char-after (point)) closing)))))
                   t)
               ((scan-error) nil))))
    (apply #'insert (nreverse closing))))

;; internal function which does most of the job

(defun close-all-parentheses* (arg &optional indent-fn)
  "Internal function which does most of the job for closing parens.

INDENT-FN is an optional indentation function to use.
ARG is command prefix arg.

See `close-all-parentheses'."
  (let* ((closing nil)
         ;; by default rely on (newline-and-indent)
         (local-indent-fn (lambda (token)
                            (newline-and-indent)
                            (insert token)))
         (indent-fn (if indent-fn
                        indent-fn
                      local-indent-fn)))
    (save-excursion
      (while (condition-case nil
                 (progn
                   (backward-up-list)
                   (let ((syntax (syntax-after (point))))
                     (cl-case (car syntax)
                       ((4) (setq closing (cons (cdr syntax) closing)))
                       ((7 8) (setq closing (cons (char-after (point)) closing)))))
                   t)
               ((scan-error) nil))))
    (dolist (token (nreverse closing))
      (if arg
          (funcall indent-fn token)
        (insert token)))))

(defun close-all-parentheses (arg)
  "Command for closing all parentheses.
Works in C-like modes.
ARG is prefix arg to control indentation."
  (interactive "P")
  (let ((my-format-fn (lambda (token)
                        ;; 125 is codepoint for '}'
                        (if (and (= token 125)
                                 ;; C, C++ and Java
                                 (member major-mode '(c-mode c++-mode java-mode)))
                            (let ((last-command-event ?}))
                              (newline)
                              (c-electric-brace nil))
                          (insert token)))))
    (close-all-parentheses* arg my-format-fn)))

(provide 'close-all-parentheses)

;;; close-all-parentheses.el ends here
