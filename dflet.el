;;; dflet.el --- dynamically-scoped flet

;; Copyright (C) 2012  Yann Hodique
;; Copyright (C) 1993, 2001-2012  Free Software Foundation, Inc.

;; Author: Yann Hodique <yann.hodique@gmail.com>
;; Keywords: lisp
;; Version: 0.1

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This is bringing back the historical definition of `flet', in all its global
;; and dynamic splendor.

;;; Code:

(require 'cl-lib)

;;;###autoload
(defmacro dflet (bindings &rest body)
  "Make temporary overriding function definitions.
This is an analogue of a dynamically scoped `let' that operates on the function
cell of FUNCs rather than their value cell.

\(fn ((FUNC ARGLIST BODY...) ...) FORM...)"
  (declare (indent 1) (debug cl-flet))
  `(cl-letf ,(mapcar
              (lambda (x)
                (list
                 (list 'symbol-function (list 'quote (car x)))
                 (cons 'lambda (cons (cadr x) (cddr x)))))
              bindings)
     ,@body))

;;;###autoload
(defmacro adflet (bindings &rest body)
  "Anaphoric version of `dflet'. Binds `this-fn' to the original
definition of the function."
  `(dflet ,(mapcar
            (lambda (x)
              (list (car x) (cadr x)
                    `(let ((this-fn ,(symbol-function (car x))))
                       ,@(cddr x))))
            bindings)
          ,@body))

(provide 'dflet)
;;; dflet.el ends here
