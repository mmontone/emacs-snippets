(load-file "publish-archive-contents.el")
(load-file "package-docgen.el")

(publish-archive-contents "archive/archive-contents" "packages.md")

(dolist (file (directory-files "." t ".*\.el$"))
  (print file)
  (package-docgen-markdown file (concat "./docs/" (file-name-base file) ".md")))

