;;; progress-buffer.el --- Package for displaying progress of tasks.  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Package for displaying progress of tasks.

;;; Code:

(defvar progress-buffer-tasks '())
(defvar progress-buffer-timer nil)

(defgroup progress-buffer nil
  "Progress buffer settings."
  ;; TODO: change parent group
  :group 'emacs)

(defcustom progress-buffer-idle-delay 0.50
  "Number of seconds of idle time to wait before updating progress buffer."
  :type 'number
  :group 'progress-buffer)

(defclass progress-buffer--task ()
  ((name :initarg :name
         :accessor progress-buffer-task-name)
   (status :initarg :status
           :initform :started
           :accessor progress-buffer-task-status)
   (status-message :initarg :status-message
                   :accessor progress-buffer-task-status-message
                   :initform nil)
   (status-data :accessor progress-buffer-task-status-data
                :initform nil)
   (total-steps :initarg :total-steps
                :initform 100
                :accessor progress-buffer-task-total-steps)
   (current-step :initarg :current-step
                 :initform 0
                 :accessor progress-buffer-task-current-step)))

(defun progress-buffer-make-task (name &optional status total-steps)
  (let ((task (make-instance 'progress-buffer--task
                             :name name
                             :status (or status :started)
                             :total-steps (or total-steps 100)
                             :current-step 1)))
    (push task progress-buffer-tasks)
    task))

(defun progress-buffer-open-buffer ()
  (interactive)
  (display-buffer (progress-buffer--get-buffer)))

(defvar progress-buffer-progress-bar-char ?█)
(defvar progress-buffer-progress-bar-background-char ?░)
(defvar progress-buffer-progress-bar-size 50)

(defun progress-buffer--display-task-progress (task)
  (with-slots (name status status-message total-steps current-step)
      task
    (let* ((completed (/ current-step (float total-steps)))
           (chars (truncate (* completed progress-buffer-progress-bar-size))))
      (insert name)
      (insert (format " [%s]" status))
      (insert (format " Completed: %s of %s (%s %%)" current-step total-steps (truncate (* completed 100))))
      (newline)

      (dotimes (_x chars)
        (insert progress-buffer-progress-bar-char))
      (dotimes (_x (- progress-buffer-progress-bar-size chars))
        (insert progress-buffer-progress-bar-background-char))
      (newline)
      (when status-message
        (let ((msg
               (cl-etypecase status-message
                 ((or symbol function)
                  (funcall status-message progress-bar))
                 (string status-message))))
          (insert msg))
        (newline))
      (newline))))

(defun progress-buffer--update-progress ()
  (when-let ((buffer (get-buffer "*progress*")))
    (with-current-buffer buffer
      (erase-buffer)
      (dolist (task progress-buffer-tasks)
        (progress-buffer--display-task-progress task)))))

(defun progress-buffer--get-buffer ()
  (let ((buffer (get-buffer-create "*progress*")))
    buffer))

(defun progress-buffer-start ()
  (interactive)
  (setq progress-buffer-timer
        ;;(run-with-idle-timer progress-buffer-idle-delay t 'progress-buffer--update-progress)
        (run-with-timer progress-buffer-idle-delay t 'progress-buffer--update-progress)

        ))

(defun progress-buffer-stop ()
  (interactive)
  (cancel-timer progress-buffer-timer)
  (setq progress-buffer-timer nil))

(defun progress-buffer-task-update (task)
  (progress-buffer--update-progress))

(defun progress-buffer-mapc-with-task (func sequence task-name &rest args)
  (progress-buffer-open-buffer)
  (let ((task (apply #'progress-buffer-make-task task-name
                     :total-steps (length sequence)
                     args)))
    (make-thread
     (lambda ()
       (condition-case err
           (progn
             (setf (progress-buffer-task-current-step task) 1)
             (setf (progress-buffer-task-status task) :in-progress)
             (dolist (elem sequence)
               (let ((result (funcall func elem)))
                 (when (stringp result)
                   (setf (progress-buffer-task-status-message task) result)))
               (cl-incf (progress-buffer-task-current-step task))
               (progress-buffer-task-update task))
             (setf (progress-buffer-task-status task) :completed)
             (progress-buffer-task-update task))
         (error
          (setf (progress-buffer-task-status task) :error)
          (setf (progress-buffer-task-status-data task) err)))))
    task))

(provide 'progress-buffer)

;;; progress-buffer.el ends here
