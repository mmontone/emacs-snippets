### log-buffer

A buffer for displaying logs

- **Version:** 0.4
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools
- **Reqs:** 

### package-docgen

Documentation generator for Emacs simple packages

- **Version:** 0.14
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, docs
- **Reqs:** 

### emacs-info

Display info about current Emacs

- **Version:** 0.4
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience, tools
- **Reqs:** 

### progress-bar-integrations

Integrations of progress-bar into Emacs.

- **Version:** 0.4
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, convenience, extensions
- **Reqs:** emacs v27, progress-bar v0.5

### progress-bar

A progress bar in the echo area

- **Version:** 0.5
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, convenience, extensions
- **Reqs:** emacs v27

### comint-eval

Minor mode with commands for comint buffer evaluation

- **Version:** 0.2
- **Author:** Marian <marianomontone@gmail.com>
- **Keywords:** processes, tools
- **Reqs:** 

### dump-emacs

Command for dumping an Emacs core

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools
- **Reqs:** 

### geiser-apropos

Apropos for Geiser

- **Version:** 0.2
- **Author:** Marian <marianomontone@gmail.com>
- **Keywords:** tools, convenience
- **Reqs:** emacs v26, geiser v0.29

### scmindex

Commands for accessing index.scheme.org

- **Version:** 0.7
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience, tools
- **Reqs:** emacs v26, request v0.3.3, at v0.3

### package-to-markdown

Create a Markdown document from an Emacs simple package file.

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, docs
- **Reqs:** emacs v26, s v1.13.1

### at

Generic accessing library

- **Version:** 0.3
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience
- **Reqs:** emacs v26, s v0.1

### close-all-parentheses

Command for closing all parenthesis at once

- **Version:** 0.1
- **Author:** Sanel Z. <sanelz@gmail.com>
- **Keywords:** 
- **Reqs:** 

### dflet

dynamically-scoped flet

- **Version:** 0.1
- **Author:** Yann Hodique <yann.hodique@gmail.com>
- **Keywords:** lisp
- **Reqs:** 

### slime-embark

Embark keymap for SLIME Lisp mode

- **Version:** 0.2
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools
- **Reqs:** 

### describe-thing-in-popup

Describe thing at point in a popup

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience
- **Reqs:** popup v0.5.9

### publish-archive-contents

Generate a Markdown file with descriptions of the package of an archive-contents

- **Version:** 0.4
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools
- **Reqs:** 

### cider-customizations

Customizations for CIDER (Clojure mode)

- **Version:** 0.3
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience, tools
- **Reqs:** emacs v26, cider v1.5.0, s v1.13.1

### move-file

Command for moving a file to a new location

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience
- **Reqs:** 

### package-list-archive

List the packages of an archive

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience
- **Reqs:** 

### package-list-topic

List packages for a given topic

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience
- **Reqs:** 

### preview-package-source

Preview package source before installing

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience, tools
- **Reqs:** 

### ert-run-test-at-point

Run ERT test at point

- **Version:** 0.3
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, testing, ert, convenience
- **Reqs:** 

### indent-buffer

Command for indenting the whole buffer

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** convenience
- **Reqs:** 

### apropos-package

Apropos command for searching Emacs packages

- **Version:** 0.3
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, package, utilities, snippet
- **Reqs:** 

