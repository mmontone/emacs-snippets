(require 'at)

(at '(:x 1) :x)
(at '((:x . 1)) :x)

(at '((:x . ((:y . 22)))) :x)
(at '((:x . ((:y . 22)))) :x :y)

(at '(1 2 3) 2)

(at '(1 2 3) 'car)

(let ((ht (make-hash-table :test 'equal)))
  (setf (gethash :x ht) 22)
  (at ht :x))

(at--subst-dot '(setq x foo.bar))
(at--subst-dot '(setq x vector.1))

(at-with-syntax
 (let ((ls (list 1 2 3)))
   ls.0))

(at-with-syntax
 (let ((bf (current-buffer)))
   bf.get-buffer-window))

(provide 'at-tests)
