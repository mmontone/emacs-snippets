;;; geiser-apropos.el --- Apropos for Geiser         -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools, convenience
;; Package-Requires: ((emacs "26") (geiser "0.29"))
;; Version: 0.2

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Apropos for Geiser.

;;; Code:

(require 'geiser)

(defun geiser-eval-expression (expr)
  "Evaluate Scheme EXPR interactively."
  (interactive "sEval expression: ")
  (geiser-eval--send
   `(:eval (:scm ,expr))
   (lambda (res)
     (let ((result (cadr (assoc 'result res))))
       (message result)))))

(defun geiser-apropos (what)
  "Apropos WHAT."
  (interactive "sApropos: ")
  (geiser-eval--send
   `(:eval (:scm ,(prin1-to-string `(apropos ,what))))
   (lambda (res)
     (let ((result (cdr (assoc 'output res))))
       (with-output-to-temp-buffer (format "*Geiser apropos: %s*" what)
         (princ result))))))

(provide 'geiser-apropos)

;;; geiser-apropos.el ends here
