# emacs-snippets

Emacs package archive with code snippets.

## Install

```lisp
(add-to-list 'package-archives '("emacs-snippets" . "https://codeberg.org/mmontone/emacs-snippets/raw/branch/master/archive/"))
```

then packages appear in `package-list-packages` .

## How to write new snippets

* Create an [Emacs simple package](https://www.gnu.org/software/emacs/manual/html_node/elisp/Simple-Packages.html) file.
* Use `auto-insert` command to fill in the basics.
* Write functions, commands and comment.
* Add `require` and `provide`.
* Add autoloads.
* Compile and load, remove warnings.
* Publish the package to `archive` directory using `package-upload-file` command.

## Packages

See the [list of packages](packages.md).

## Other snippets

### Custom error display

```lisp
(defun debugger-insert-error-message (args)
  "Insert debugger errors in a different way."
  (when (eql (car args) 'error)
    (let ((error (second args)))
      (insert (error-message-string error))
      (newline 2))))

(advice-add 'debugger--insert-header :before #'debugger-insert-error-message)
```

## Maintainance

After adding or updating a package, update docs with:

```
(publish-archive-contents
 "~/src/emacs-snippets/archive/archive-contents"
 "~/src/emacs-snippets/packages.md")
 ```
