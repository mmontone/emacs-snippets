all: packages.md

packages.md:
	emacs -q --batch -l scripts/gendoc.el

clean:
	rm -f packages.md

rebuild: clean all
