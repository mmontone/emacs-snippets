;;; slime-embark.el --- Embark keymap for SLIME Lisp mode  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mariano Montone

;; Author: Mariano Montone(require 'slime) <marianomontone@gmail.com>
;; Keywords: tools
;; Version: 0.2

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Embark keymap for SLIME Lisp mode.

;;; Code:

(require 'slime)
(require 'embark)

(defun slime-embark--identifier-types (identifier)
  "Return list of target types appropriate for IDENTIFIER."
  (let ((symbol-name (slime-qualify-cl-symbol-name (substring-no-properties identifier))))
    (let* ((types
            (slime-eval
             `(cl:let ((symbol (cl:read-from-string ,symbol-name)))
                      (cl:remove nil (cl:list
                                      (cl:and (cl:boundp symbol) "variable")
                                      (cl:and (cl:fboundp symbol) "function")
                                      (cl:and (cl:find-class symbol nil) "class")
                                      (cl:and (cl:find-package symbol) "package")))))))
      (or (mapcar #'intern types) '(symbol)))))

(defun slime-embark-target-identifier-at-point ()
  "Target identifier at point.

In Emacs Lisp and IELM buffers the identifier is promoted to a
symbol, for which more actions are available.  Identifiers are
also promoted to symbols if they are interned Emacs Lisp symbols
and found in a buffer in a major mode that is not derived from
`prog-mode' (this is intended for when you might be reading or
writing about Emacs).

As a convenience, in Org Mode an initial ' or surrounding == or
~~ are removed."
  (when-let (bounds (bounds-of-thing-at-point 'symbol))
    (let ((name (buffer-substring (car bounds) (cdr bounds))))
      (when (derived-mode-p 'org-mode)
        (cond ((string-prefix-p "'" name)
               (setq name (substring name 1))
               (cl-incf (car bounds)))
              ((string-match-p "^\\([=~]\\).*\\1$" name)
               (setq name (substring name 1 -1))
               (cl-incf (car bounds))
               (cl-decf (cdr bounds)))))
      (mapcar (lambda (type) `(,type ,name . ,bounds))
              (slime-embark--identifier-types name)))))

(defvar slime-embark-keymap-alist
  '((file embark-file-map)
    (library embark-library-map)
    (environment-variables embark-file-map) ; they come up in file completion
    (url embark-url-map)
    (email embark-email-map)
    (buffer embark-buffer-map)
    (tab embark-tab-map)
    (expression slime-embark-expression-map)
    (identifier slime-embark-identifier-map)
    (defun slime-embark-defun-map)
    (symbol slime-embark-symbol-map)
                                        ;(face embark-face-map)
                                        ;(command embark-command-map)
    ;;(variable slime-embark-variable-map)
    (function slime-embark-function-map)
    (minor-mode embark-command-map)
    (unicode-name embark-unicode-name-map)
                                        ;(package embark-package-map)
    (bookmark embark-bookmark-map)
    (region embark-region-map)
    (sentence embark-sentence-map)
    (paragraph embark-paragraph-map)
    (kill-ring embark-kill-ring-map)
    (heading embark-heading-map)
    (t embark-general-map)))

(defvar-keymap slime-embark-identifier-map
  :doc "Keymap for Embark identifier actions."
  :parent embark-general-map
  "RET" #'slime-edit-definition
  "h" #'slime-describe-symbol
  "H" #'embark-toggle-highlight
  "d" #'slime-edit-definition
  "r" #'slime-edit-uses
  "a" #'slime-apropos
  "s" #'info-lookup-symbol
  "n" #'embark-next-symbol
  "p" #'embark-previous-symbol
  "'" #'expand-abbrev
  "$" #'ispell-word
  "o" #'occur)

(defvar-keymap slime-embark-defun-map
  :doc "Keymap for Embark defun actions."
  :parent embark-expression-map
  "RET" #'slime-pprint-eval-region
  "e" #'slime-eval-defun
  "c" #'slime-compile-defun
  ;;"l" #'elint-defun
  ;;"D" #'edebug-defun
  "o" #'checkdoc-defun
  "N" #'narrow-to-defun)

(defvar-keymap slime-embark-expression-map
  :doc "Keymap for Embark expression actions."
  :parent embark-general-map
  "RET" #'slime-pprint-eval-last-expression
  "e" #'slime-pprint-eval-last-expression
  ;;"<" #'embark-eval-replace
  "m" #'slime-macroexpand-all
  "TAB" #'indent-sexp
  "r" #'raise-sexp
  "t" #'transpose-sexps
  "k" #'kill-region
  "u" #'backward-up-list
  "n" #'forward-list
  "p" #'backward-list)

(defvar-keymap slime-embark-symbol-map
  :doc "Keymap for Embark symbol actions."
  :parent slime-embark-identifier-map
  "RET" #'slime-edit-definition
  "h" #'slime-describe-symbol
  ;;"s" #'embark-info-lookup-symbol
  "d" #'slime-edit-definition
  "e" #'slime-eval-last-expression
  "a" #'slime-apropos
  "\\" #'embark-history-remove)

(defvar-keymap slime-embark-function-map
  :doc "Keymap for Embark function actions."
  :parent slime-embark-symbol-map
  ;;"m" #'elp-instrument-function ;; m=measure
  ;;"M" 'elp-restore-function ;; quoted, not autoloaded
  ;;"k" #'slime-debug-on-entry ;; breaKpoint (running out of letters, really)
  ;;"K" #'cancel-debug-on-entry
  "t" #'slime-toggle-trace-fdefinition
  "T" #'slime-untrace-all
  "p" #'slime-toggle-profile-fdefinition
  "P" #'slime-unprofile-all)

(defvar slime-embark-target-finders
  '(embark-target-top-minibuffer-completion
    embark-target-active-region
    embark-target-text-heading-at-point
    embark-target-collect-candidate
    embark-target-completion-at-point
    embark-target-bug-reference-at-point
    embark-target-package-at-point
    embark-target-email-at-point
    embark-target-url-at-point
    embark-target-file-at-point
    embark-target-custom-variable-at-point
    slime-embark-target-identifier-at-point
    embark-target-library-file-at-point
    embark-target-expression-at-point
    embark-target-sentence-at-point
    embark-target-paragraph-at-point
    embark-target-defun-at-point
    embark-target-prog-heading-at-point))

(add-hook 'slime-mode-hook
          (lambda ()
            (setq-local embark-target-finders slime-embark-target-finders)
            (setq-local embark-keymap-alist slime-embark-keymap-alist)))

(provide 'slime-embark)
;;; slime-embark.el ends here
