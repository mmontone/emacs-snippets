;;; package-list-topic.el --- List packages for a given topic  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marian

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: convenience
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; List packages for a given topic.

;; Use: M-x `package-list-topic'.

;;; Code:

(require 'package)

(defun package-list-topic (topic)
  "List packages for TOPIC."
  (interactive "sTopic: ")
  (package-show-package-list t (list topic)))

(provide 'package-list-topic)

;;; package-list-topic.el ends here
